
/*
 * En development: Generar el perfil específico
 * En production: Generar y descargar de nuevo el certificado
 */

var bbdd = require('bbdd');

module.exports = function(f_callback) {
	
	var Cloud = require('ti.cloud');
	var user_device_token = Ti.App.Properties.getString("device_token", null);
	//var username = Ti.App.Properties.getDouble('device_id');
	var password = 'DKJ3aD8JmV5Nbcd73';
	
	getDeviceToken();
	
	//REGISTER LOCAL PUSH NOTIFICATION HERE
	function getDeviceToken() {
		
		if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
		    function registerForPush() {
		        Ti.Network.registerForPushNotifications({
		            success: deviceTokenSuccess,
		            error: deviceTokenError,
		            callback: receivePush
		        });
		        // Remove event listener once registered for push notifications
		        Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush); 
		    };
		 
			// Wait for user settings to be registered before registering for push notifications
		    Ti.App.iOS.addEventListener('usernotificationsettings', registerForPush);
		 
		    // Register notification types to use
		    Ti.App.iOS.registerUserNotificationSettings({
			    types: [
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
		            Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
		        ]
		    });
		
		} else {
		    // For iOS 7 and earlier
		    Ti.Network.registerForPushNotifications({
		        // Specifies which notifications to receive
		        types: [
		            Ti.Network.NOTIFICATION_TYPE_BADGE,
		            Ti.Network.NOTIFICATION_TYPE_ALERT,
		            Ti.Network.NOTIFICATION_TYPE_SOUND
		        ],
		        success: deviceTokenSuccess,
		        error: deviceTokenError,
		        callback: receivePush
		    });
		}
		
		function deviceTokenSuccess(e) {
	        user_device_token = e.deviceToken;
	        Ti.App.Properties.setString("device_token", user_device_token);
			Ti.API.info("Device token: " + user_device_token);
			registerUser();
		}
	    function deviceTokenError(e) {
	        Ti.API.info("Error during registration: " + e.error);
	    }
		function receivePush(e) {
      		//var badge = e.data.badge;
			//Ti.UI.iPhone.appBadge = badge;
			//Ti.Media.vibrate();
			f_callback(e.data);
		}
	}
	
	//REGISTER USER ON CLOUD
	function registerUser() {
		Cloud.Users.create({
		    username: user_device_token,
		    password: password,
		    password_confirmation: password,
		    first_name: "Firstname",
		    last_name: "Lastname"
		}, function (e) {
		    if (e.success) {
		    	Ti.API.info('registerUser OK');
		    } else {
		    	Ti.API.info('error registering user');
		    }
		    login();
		});
	}
	
	//LOGIN TO CLOUD AS A USER THAT WE CREATED BEFORE
	function login(){
		Cloud.Users.login({
		    login: user_device_token,
		    password: password
		}, function (e) {
		    if (e.success) {
		        var user = e.users[0].id;
		        Ti.App.Properties.setString("cloud_user_id", user);
				new bbdd({
					device_token:user
				}, 'devices/add');
		        Ti.API.info('Login OK');
		        subscribeToServerPush();
		    } else {
		        Ti.API.info('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
		    }
		});
	}
	
	//REGISTER SERVER PUSH 
	function subscribeToServerPush(){
		Cloud.PushNotifications.subscribe({
	    	channel: 'notifications',
	    	type: 'ios',
	    	device_token: user_device_token
		}, function (e) {
		    if (e.success) {
		    	Ti.API.info('Success'+((e.error && e.message) || JSON.stringify(e)));
		    	/*
		    	var path = Alloy.CFG.url + 'devices/register_cloud';
				var client = Ti.Network.createHTTPClient({
					onload: function(e) {
						Ti.API.info('ok');
						var result = JSON.parse(this.responseText);
						//alert(result);
						if (result['status'] != 'ok') {
							alert('error al registrar el dispositivo para notificaciones');
						}
					},
					onerror: function(e) {
						Ti.API.info('error registrando');
						//alert('error')
					},
					timeout: 15000
				});
				
				client.open('POST', path);
				client.send({
					device_id:Ti.App.Properties.getDouble('device_id', null),
					cloud_id:Ti.App.Properties.getString('cloud_user_id', null)
				});
				*/
				
		    } else {
		        Ti.API.info('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
		    }
		});
	}
	
};