module.exports = function(args, path, callback) {
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info(path + ' --- ' + JSON.stringify(args) + ' --- ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				if (callback) {
					callback(result.data);
				}
			}
			if (result.error) {
				Ti.UI.createAlertDialog({
					title:L('alert'),
					message:result.error,
					ok:L('ok')
				}).show();
			}
		},
		onerror: function(e) {
			// alert(e);
			Ti.UI.createAlertDialog({
				title: L('alert'),
				message: L('connection_error'),
				ok: L('ok')
			}).show();
		}
	});
	
	args.token = Ti.App.Properties.getString("cloud_user_id", 'prueba');
	
	client.open('POST', Alloy.CFG.url + path);
	client.send(args);

};
