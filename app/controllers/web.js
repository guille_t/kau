var args = arguments[0] || {};

$.web.url = args;

$.web.addEventListener('load', function() {

	$.loading.hide();
	$.reload.show();

	if (!$.web.canGoBack()) {
		$.back.enabled = false;
	} else {
		$.back.enabled = true;
	}
	if (!$.web.canGoForward()) {
		$.fwd.enabled = false;
	} else {
		$.fwd.enabled = true;
	}

});

$.back.addEventListener('click', function() {
	if ($.web.canGoBack()) {
		$.web.goBack();
	}
});

$.fwd.addEventListener('click', function() {
	if ($.web.canGoForward()) {
		$.web.goForward();
	}
});

$.reload.addEventListener('click', function() {
	$.web.reload();
});

$.web.addEventListener('beforeload', function() {
	$.loading.show();
	$.reload.hide();
});

$.open.addEventListener('click', function() {
	Ti.Platform.openURL($.web.url);
});

$.close.addEventListener('click', function() {
	$.win.close({left:'100%'});
});
