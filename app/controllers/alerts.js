var args = arguments[0] || {};

$.header.text = args.header;
$.title.text = args.title;
$.text.text = args.text;

if (args.text2) {
	$.text2.text = args.text2;
} else {
	$.alerts.remove($.text2);
}

for (var i in args.buttons) {
	var button = Ti.UI.createButton({
		backgroundColor:Alloy.CFG.orange,
		color:'#FFF',
		backgroundImage:null,
		top:'20dp',
		width:'70%',
		title:args.buttons[i].label
	});
	$.scrollview.add(button);
	button.addEventListener('click', args.buttons[i].callback);

	if (args.buttons[i].label == L('close')) {
		var close = Ti.UI.createView({
			width:'50dp',
			height:'50dp',
			right:0,
			top:0
		});
		close.add(Ti.UI.createLabel({text:'X', color:'#FFF', font:{fontSize:'15dp'}}));
		$.view.add(close);
		close.addEventListener('singletap', args.buttons[i].callback);
	}
}

$.scrollview.add(Ti.UI.createView({height:'20dp'}));
