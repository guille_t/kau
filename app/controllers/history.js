var args = arguments[0] || {};

$.header.setWin($.win);
$.header.setTitle(L('historical'));

var bbdd = require('bbdd');

$.t1.text = L('ticker');
$.t3.text = L('stored');

if (args.type == 'St') {
	$.t2.text = L('position_l');
	new bbdd({ticker: args.ticker}, 'historicals/getSt', setData);
} else {
	$.t2.text = L('move');
	new bbdd({type: args.type}, 'historicals/getQv2', setData);
}

function setData(data) {
	if (data.length == 0) {
		$.table.hide();
		$.win.add(Ti.UI.createLabel({
			text: L('no_data'),
			color: "#000",
			font:{
				fontFamily:Alloy.CFG.fontFamily,
				fontSize:Alloy.CFG.fontSize
			}
		}));	
		return;
	}
	for (var i in data) {
		var row = Ti.UI.createTableViewRow({
			height: '70dp',
			selectionStyle: OS_IOS ? Ti.UI.iPhone.TableViewCellSelectionStyle.NONE : ''
		});
		var view = Ti.UI.createView({
			backgroundColor:'#E3E1DD',
			left:'1%',
			right:'1%',
			top:'3dp',
			bottom:'3dp',
		});
		row.add(view);
		view.add(Ti.UI.createLabel({
			text: args.type == 'St' ? data[i].Historical.ticker : data[i].in.ticker,
			left: '5%',
			color: "#000",
			font:{
				fontFamily:Alloy.CFG.fontFamilyBold,
				fontSize:Alloy.CFG.fontSizeSmaller
			}
		}));
		if (args.type == 'St') {
			var aux = data[i].Historical.created.split(' ');
			var aux1 = aux[0].split('-');
			var aux2 = aux[1].split(':');
			var date = new Date(aux1[0], aux1[1] - 1, aux1[2] - 1, aux2[0], aux2[1], aux2[2]);
			date = zerofill(date.getDate()) + '-' + zerofill(date.getMonth() + 1);
			view.add(Ti.UI.createLabel({
				text: (data[i].Historical.position == 'long' ? L('long_t') : L('short_t')) + ' (' + date + ')',
				color: "#000",
				font:{
					fontFamily:Alloy.CFG.fontFamily,
					fontSize:Alloy.CFG.fontSize
				}
			}));
		} else {
			var aux = data[i].in.created.split(' ');
			var aux1 = aux[0].split('-');
			var aux2 = aux[1].split(':');
			var date1 = new Date(aux1[0], aux1[1] - 1, aux1[2] - 1, aux2[0], aux2[1], aux2[2]);
			date1 = zerofill(date1.getDate()) + '-' + zerofill(date1.getMonth() + 1);
			
			var aux = data[i].out.created.split(' ');
			var aux1 = aux[0].split('-');
			var aux2 = aux[1].split(':');
			var date2 = new Date(aux1[0], aux1[1] - 1, aux1[2] - 1, aux2[0], aux2[1], aux2[2]);
			date2 = zerofill(date2.getDate()) + '-' + zerofill(date2.getMonth() + 1);
			
			view.add(Ti.UI.createLabel({
				top: '10dp',
				text: L('in') + ' (' + date1 + ')',
				color: Alloy.CFG.green,
				font: {
					fontFamily:Alloy.CFG.fontFamily,
					fontSize:Alloy.CFG.fontSize
				}
			}));
			view.add(Ti.UI.createLabel({
				bottom: '10dp',
				text: L('out') + ' (' + date2 + ')',
				color: Alloy.CFG.red,
				font: {
					fontFamily:Alloy.CFG.fontFamily,
					fontSize:Alloy.CFG.fontSize
				}
			}));
		}
		var stored = args.type == 'St' ? data[i].Historical.stored : data[i].out.stored;
		view.add(Ti.UI.createLabel({
			text: Math.round(stored * 100) / 100 + '%',
			right: '5%',
			color: "#000",
			font:{
				fontFamily:Alloy.CFG.fontFamilyBold,
				fontSize:Alloy.CFG.fontSizeSmaller
			}
		}));
		$.table.appendRow(row);
	}
}

function zerofill(a) {
	return a < 10 ? '0' + a : a;
}
