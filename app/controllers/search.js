var bbdd = require('bbdd');
var args = arguments[0] || {};

$.header.setWin($.win);
$.header.setTitle(L('analysis_values'));

$.search_table.top = parseInt(Alloy.CFG.headerHeight) + 43 + 'dp';

$.search.addEventListener('return', function(e) {
	$.search_table.data = [];
	$.loading.show();
	
	var url = 'http://d.yimg.com/autoc.finance.yahoo.com/autoc?region=ES&query=' + $.search.value + '&callback=YAHOO.Finance.SymbolSuggest.ssCallback';
	
	var client = Ti.Network.createHTTPClient({
		onload:function() {
			var result = this.responseText.replace('YAHOO.Finance.SymbolSuggest.ssCallback(', '');
			result = result.substring(0, result.length - 1);
			Ti.API.info(result);
			result = JSON.parse(result);
			var items = result.ResultSet.Result;
			
			if (items.length == 0) {
				Ti.UI.createAlertDialog({
					title:L('alert'),
					message:L('error_yahoo'),
					ok:L('ok')
				}).show();
			}
			
			for (var i in items) {
				var row = Ti.UI.createTableViewRow({
					_symbol:items[i].symbol,
					height:'50dp'
				});
				row.add(Ti.UI.createLabel({
					text:items[i].name + ' (' + items[i].symbol + ')',
					color: "#333",
					font:{
						fontFamily:Alloy.CFG.fontFamily,
						fontSize:Alloy.CFG.fontSizeSmall
					},
					left:'10%',
					right:'10%'
				}));
				$.search_table.appendRow(row);
				Ti.API.info(row);
			}
			
			$.loading.hide();
	
		}
	});
	client.open('GET', url);
	client.send();
	$.search.blur();
});

$.search_table.addEventListener('click', function(e) {
	$.loading.show();
	$.search_table.touchEnabled = false;
	new bbdd({ticker: e.row._symbol}, 'sts/get_info', function(d) {
		var win = Alloy.createController('analysis', d).getView();
		win.open({left: 0});
		$.loading.hide();
		$.search_table.touchEnabled = true;
	});
});