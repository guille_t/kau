var url = '';

var bbdd = require('bbdd');
new bbdd({}, 'ads/ad', callback);

function callback(data) {
	if (data) {
		var rand = new Date(data.Ad.modified.replace(/-/g, '/')).getTime();
		$.ads.image = Alloy.CFG.url + 'images/get/' + data.Ad.id + '/640x174?v=' + rand;
		url = data.Ad.url;
	}
}

$.ads.addEventListener('singletap', function() {
	var web = Alloy.createController('web', url).getView();
	web.open({left:0});
});