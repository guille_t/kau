var args = arguments[0] || {};

if (args == 'know_more') {
	var text = 'Conoce a fondo estas estrategias de la mano de un coach personal y descubre todo el potencial de KA\'U+.';
	var alert_data = {
		header:'Swing Trading',
		title:'Saber más',
		text:text,
		buttons:[
			/*{
				label: L('tutorial'),
				callback: function() {
					var web = Alloy.createController('web', 'http://www.kauapp.com').getView();
					web.open({left:0});
				}
			}*/
		]
	};
	$.header.setHelp(alert_data);
} else {
	var text = 'Conoce a fondo la estrategia de Quantitative Value de la mano de un coach personal y descubre todo el potencial de KA\'U+.';
	var alert_data = {
		header:'Quantitative Value',
		title:'Saber más',
		text:text,
		buttons:[
			/*{
				label: L('tutorial'),
				callback: function() {
					var web = Alloy.createController('web', 'http://www.kauapp.com').getView();
					web.open({left:0});
				}
			}*/
		]
	};
	$.header.setHelp(alert_data);
}

$.explain.text = text;

var bbdd = require('bbdd');

$.header.setWin($.win);
$.header.setTitle(L('know_more'));

$.f_name.addEventListener('singletap', function() {
	$.name.focus();
});
$.f_email.addEventListener('singletap', function() {
	$.email.focus();
});
$.f_phone.addEventListener('singletap', function() {
	$.phone.focus();
});
$.f_message.addEventListener('singletap', function() {
	$.message.focus();
});

if (args == 'qv_know_more') {
	var section = 11;
} else {
	var section = 5;
}

if (Alloy.CFG.places[section].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.scrollview.bottom = ads.height;
}

$.submit.addEventListener('click', function() {
	$.submit.hide();
	$.loading.show();
	var error = false;
	if (!$.name.value) {
		error = true;
	}
	if (!$.email.value || !isEmail(trim($.email.value))) {
		error = true;
	}
	if (!$.phone.value) {
		error = true;
	}
	if (!$.message.value) {
		error = true;
	}
	
	if (error) {
		Ti.UI.createAlertDialog({
			title:L('alert'),
			message:L('form_error'),
			ok:L('ok')
		}).show();
		
		$.submit.show();
		$.loading.hide();
		return;
	}
	
	new bbdd({
		name:trim($.name.value),
		subject:args == 'know_more' ? 'ST' : 'QV',
		email:trim($.email.value),
		phone:trim($.phone.value),
		message:trim($.message.value),
	}, 'devices/contact', function() {
		Ti.UI.createAlertDialog({
			title:L('sended'),
			message:L('thanks_for_contact'),
			ok:L('close')
		}).show();
		$.win.close({left:'100%'});
		$.submit.show();
		$.loading.hide();
	});
	
});


function isEmail(_email) {
	var emailReg = /^[a-z][a-z-_0-9\.]+@[a-z-_=>0-9\.]+\.[a-z]{2,3}$/i;
	return emailReg.test(_email);
}
function trim(x) {
	return x.replace(/^\s+|\s+$/gm,'');
}
