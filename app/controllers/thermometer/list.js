var args = arguments[0] || {};

var bbdd = require('bbdd');

$.header.setWin($.win);
$.header.setTitle(L(args));

$.loading.show();

switch (args) {
	case 'markets_doing':
		new bbdd({type: 'trend'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '1';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section1.add(row);
			}
			finished();
		});
		new bbdd({type: 'markets'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '2';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section2.add(row);
			}
			finished();
		});
		new bbdd({type: 'markets2'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '3';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section3.add(row);
			}
			finished();
		});
		$.title1.text = L('trend');
		$.name1.text = L('name');
		$.ticker1.text = L('ticker');
		$.position1.text = L('position_l');
		
		$.title2.text = L('markets');
		$.name2.text = L('name');
		$.position2.text = L('percent');
		
		$.title3.text = L('markets2');
		$.name3.text = L('name');
		$.position3.text = L('percent');
		break;
	case 'users_doing':
		new bbdd({type: 'followed'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '1';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section1.add(row);
			}
			finished();
		});
		new bbdd({type: 'followed2'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '2';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section2.add(row);
			}
			finished();
		});
		new bbdd({type: 'position'}, 'thermometers/get', function(data) {
			for (var i in data) {
				data[i].type = args + '3';
				var row = Alloy.createController('thermometer/row', data[i]).getView();
				$.section3.add(row);
			}
			finished();
		});
		$.title1.text = L('followed') + ' Swing Trading';
		$.name1.text = L('name');
		$.ticker1.text = L('ticker');
		$.position1.text = L('num');
		
		$.title2.text = L('followed') + ' Quantitative Value';
		$.name2.text = L('name');
		$.ticker2.text = L('ticker');
		$.position2.text = L('num');
		
		$.title3.text = L('position_market');
		$.name3.text = L('type');
		$.position3.text = L('num');
		break;
}
var steps = 0;
function finished() {
	steps++;
	if (steps >= 3) {
		$.win.add($.table);
		$.loading.hide();
	}
}