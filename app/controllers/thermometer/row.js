var args = arguments[0] || {};

switch (args.type) {
	case 'markets_doing1':
		if (args.St) {
			$.name.text = args.St.title;
			$.ticker.text = args.St.ticker;
			$.position.text = args.St.position == 'long' ? L('up') : L('down');
		}
		break;
	case 'markets_doing2':
	case 'markets_doing3':
		$.name.text = args.title;
		$.position.text = args.percent + '%';
		//$.position.text = Math.round(args.total);
		break;
	case 'users_doing1':
	case 'users_doing2':
		$.name.text = args.title;
		$.ticker.text = args.ticker;
		$.position.text = args.num;
		break;
	case 'users_doing3':
		$.name.text = args.title;
		$.position.text = args.num;
		break;
}