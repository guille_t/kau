var args = arguments[0] || {};

var percent = ((args.cot / args.close - 1) * 100).toFixed(2);
if (percent >= 0) {
	percent = '+' + percent;
}

$.name.text = args.title;
$.cot.text = args.cot + ' (' + percent + '%)';