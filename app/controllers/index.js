var bbdd = require('bbdd');

$.header.setWin($.win);

new bbdd({}, 'devices/demo', demo);
function demo(data) {
	Ti.App.Properties.setInt('demo', data);
	$.header.hideBack(profile);
	new bbdd({}, 'places/places', callback);
}

if (OS_IOS) {
	var Cloud = require('cloud_ios');
} else {
	var Cloud = require('cloud_android');
}

function notification_callback(data) {
	//alert(data.alert);
	var notification = {
		header:L('alert'),
		title:data.alert,
		text:'',
		buttons:[{
			label: L('close'),
			callback: function() {
				not.animate({opacity:0}, function() {
					$.win.remove(not);
				});
			}
		}]
	};
	var not = Alloy.createController('alerts', notification).getView();
	$.win.add(not);
	not.animate({opacity:1});
	if (OS_IOS) {
		Ti.UI.iPhone.appBadge = 0;
	}
}

if (Ti.App.Properties.getString('user_id', null)) {
	new bbdd({user_id:Ti.App.Properties.getString('user_id')}, 'users/active', function(data) {
		if (!data) {
			Ti.App.Properties.removeProperty('user_id');
		}
	});
}

function profile() {
	var options = Alloy.createController('options').getView();
	options.open({left:0});
}


var f = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'license.txt');
var contents = f.read();
var f = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'license2.txt');
var contents2 = f.read();

var f = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'legal.txt');
var legal = f.read();

var alertDialog = Alloy.createController('alerts', {
	header: L('license'),
	title: 'Términos y condiciones adicionales aplicables a usuarios de la licencia "KA\'U+"',
	text: contents.text,
	text2: contents2.text,
	buttons: [
		{
			label: L('accept'),
			callback: function() {
				$.win.remove(alertDialog);
				Ti.App.Properties.setBool('license_accepted', true);
			}
		}
	]
}).getView();

function callback(data) {
	Alloy.CFG.places = data;
	
	if (Alloy.CFG.places[1].status) {
		var ads = Alloy.createController('ads').getView();
		$.win.add(ads);
		$.table.bottom = ads.height;
	}
	
	$.win.open();
	
	if (Ti.App.Properties.getBool('help_welcome', false) == false) {
		setTimeout(function() {
			$.header.showHelp();
		}, 1500);
	} else {
		new Cloud(notification_callback, $.win);
	}
	
	if (Ti.App.Properties.getInt('demo') && Ti.App.Properties.getBool('license_accepted', false) == false) {
		$.win.add(alertDialog);
		alertDialog.animate({opacity: 1});
	}
}

$.win.addEventListener('open', function() {
	$.win.isOpened = true;
});

var alert_data = {
	header:'Bienvenido a KAU+',
	title:'Gracias por descargar la aplicación de KAU+',
	text:'Gracias por descargar nuestra app de estrategias de inversión en bolsa. En ella encontrarás 2 estrategias útiles y efectivas y todo lo que necesitas para gestionar tus inversiones de la forma más rentable y segura.\n\nPara conocer cómo funcionan las estrategias y sacarles todo el partido puedes acceder a nuestras ayudas en cada pantalla (i) o ver nuestros videotutoriales.\n\nTambién puedes contactar con nosotros a través del formulario de “saber más” y descubrir todo el potencial de las estrategias de inversión que tienes en tu mano con Kau Finanzas y su equipo de coaching.\n\nTe informamos así mismo que todo el contenido de esta aplicación tiene una finalidad informativa y bajo ninguna circunstancia debes entenderla como asesoramiento financiero, oferta de compra, venta, suscripción o negociación de valores o de otros instrumentos.',
	buttons:[
		// {
			// label: L('tutorial'),
			// callback: function() {
				// var web = Alloy.createController('web', 'http://www.kauapp.com').getView();
				// web.open({left:0});
			// }
		// },
		{
			label: L('legal_advice'),
			callback: function() {
				var legalAlert = Alloy.createController('alerts', {
					header: 'KA\'U+',
					title: 'Aviso Legal',
					text: legal.text,
					buttons: [
						{
							label: L('close'),
							callback: function() {
								legalAlert.animate({opacity: 0}, function() {
									$.win.remove(legalAlert);
								});
							}
						}
					]
				}).getView();
				$.win.add(legalAlert);
				legalAlert.animate({opacity: 1});
			}
		},
		{
			label: L('close'),
			callback: function() {
				if (Ti.App.Properties.getBool('help_welcome', false) == false) {
					new Cloud(notification_callback, $.win);
					Ti.App.Properties.setBool('help_welcome', true);
				}
			}
		}
	]
};

$.header.setHelp(alert_data);

$.table.addEventListener('click', function(e) {
	
	switch(e.row.id) {
		case 'finder':
			var swing = Alloy.createController('swing/finder', e.row.id).getView();
			swing.open({left:0});
			break;
		case 'qv_model':
			var swing = Alloy.createController('qv/models', e.row.id).getView();
			swing.open({left:0});
			break;
		case 'qv_wallet':
			var swing = Alloy.createController('qv/wallet', {from:'qv_wallet'}).getView();
			swing.open({left:0});
			break;
		case 'model':
		case 'wallet':
			// var swing = Alloy.createController('swing/wallet', e.row.id).getView();
			// swing.open({left:0});
			var swing = Alloy.createController('types', e.row.id).getView();
			swing.open({left:0});
			break;
		case 'radar':
			var swing = Alloy.createController('qv/radar', e.row.id).getView();
			swing.open({left:0});
			break;
		case 'know_more':
		case 'qv_know_more':
			var contact = Alloy.createController('contact', e.row.id).getView();
			contact.open({left:0});
			break;
		case 'training':
		case 'qv_training':
			var contact = Alloy.createController('training', e.row.id).getView();
			contact.open({left:0});
			break;
		case 'markets_doing':
		case 'users_doing':
			var win = Alloy.createController('thermometer/list', e.row.id).getView();
			win.open({left:0});
			break;
		case 'analysis':
			var win = Alloy.createController('search').getView();
			win.open({left:0});
			break;
		case 'quotes':
			var win = Alloy.createController('quotes').getView();
			win.open({left:0});
			break;
	}
	
});