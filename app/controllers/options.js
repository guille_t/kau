var args = arguments[0] || {};

var bbdd = require('bbdd');

$.header.setWin($.win);
$.header.setTitle(L('profile'));

if (Ti.App.Properties.getString('user_id', null)) {
	$.no_logged.visible = false;
} else {
	$.logged.visible = false;
}

$.submit.addEventListener('click', function() {
	$.submit.hide();
	$.loading.show();
	setTimeout(function() {
		if (!Ti.App.Properties.getString('user_id', null)) {
			$.loading.hide();
			$.submit.show();
		}
	}, 2000);
	new bbdd({code:$.login.value}, 'devices/login', function(data) {
		if (data) {
			Ti.App.Properties.setString('user_id', data);
			$.no_logged.visible = false;
			$.logged.visible = true;
			$.loading.hide();
		}
	});
});

$.logout.addEventListener('click', function() {
	Ti.App.Properties.removeProperty('user_id');
	$.no_logged.visible = true;
	$.logged.visible = false;
});