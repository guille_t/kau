var args = arguments[0] || {};

$.web.scalesPageToFit = false;

if (args.type == 'St') {
	$.web.url = 'https://chart.finance.yahoo.com/t?s=' + args.ticker + '&lang=es-ES&region=ES&width=300&height=180';
} else {
	$.web.url = Alloy.CFG.url + 'qvs/graph/' + args.ticker.replace(':', '*');
}