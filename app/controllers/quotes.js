var bbdd = require('bbdd');
var args = arguments[0] || {};

$.header.setWin($.win);
$.header.setTitle(L('quotes'));

load();
$.loading.show();

function load() {
	
	new bbdd({}, 'sts/marquee', function (data) {
	
		if (!data) return;
		
		for (var i in data) {
			
			var row = Alloy.createController('quotes_row', data[i]).getView();
			row._data = data[i];
			$.table.appendRow(row);
			
		}
		
		$.loading.show();
	
	});
	
}

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('view', {ticker: e.row._data.ticker, from: 'St'}).getView();
	win.open({left: 0});	
});