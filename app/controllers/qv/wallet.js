var args = arguments[0] || {};

if (args.from == 'model') {
	switch (args.type) {
		case 'eu_long':
			var alert_data = {
				header:'Quantitative Value',
				title:'Cartera Largos Europa',
				text:'La cartera Europa Largos se compone de 10 valores elegidos entre aquellos que nuestros radares han seleccionado como los mejores en el ámbito europeo.\n\nTanto la selección de los 10 valores que la componen como la rotación que hacemos de ellos se realiza de forma automática según el criterio de calidad que nos indica qué valores deben formar parte de la cartera y cuál o cuáles han cambiado la relación entre precio y valor y por lo tanto deben abandonarla.\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el acceso a las acciones que componen nuestra cartera modelo y en vez del nombre de los valores verás la leyenda (pro).\n\nSi ya eres cliente de Kau Finanzas tendrás acceso libre a todas las acciones que componen nuestra cartera.\n\nLa rentabilidad de la cartera modelo Largos Europa se presenta con un apalancamiento del 1,5:1 y se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo nuestra cartera modelo de Largos Europa.',
				buttons:[
					{
						label: L('tutorial'),
						callback: function() {
							var web = Alloy.createController('web', 'http://vimeo.com/112182684').getView();
							web.open({left:0});
						}
					}
				]
			};
			break;
		case 'usa_long':
			var alert_data = {
				header:'Quantitative Value',
				title:'Cartera Largos América',
				text:'La cartera América Largos se compone de 10 valores elegidos entre aquello que nuestros radares han seleccionado como los mejores del ámbito americano.\n\nTanto la selección de los 10 valores que la componen como la rotación que hacemos de ellos se realiza de forma automática según el criterio de calidad que nos indica qué valores deben formar parte de la cartera y cuál o cuáles han cambiado la relación entre precio y valor y por lo tanto deben abandonarla.\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el acceso a las acciones que componen nuestra cartera modelo y en vez del nombre de los valores verás la leyenda (pro).\n\nSi ya eres cliente de Kau Finanzas tendrás acceso libre a todas las acciones que componen nuestra cartera.\n\nLa rentabilidad de la cartera modelo Largos América se presenta con un apalancamiento del 1,5:1 y se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo nuestras cartera modelo de largos américa.',
				buttons:[
					{
						label: L('tutorial'),
						callback: function() {
							var web = Alloy.createController('web', 'http://vimeo.com/112196524').getView();
							web.open({left:0});
						}
					}
				]
			};
			break;
		case 'eu_long_short':
			var alert_data = {
				header:'Quantitative Value',
				title:'Long/Short Europa',
				text:'La cartera largos/cortos Europa se compone de un total de 16 valores.\n\n12 de ellos elegidos entre aquellos que nuestros radares han seleccionado automáticamente como los mejores valores europeos y 6 elegidos entre aquellos que nuestros radares han seleccionado como los peores.\n\nDe esta forma hemos creado una cartera de retorno absoluto basada en criterios de valor, con una composición 120% largos-60% cortos, y una exposición neta alcista de un 60%\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el acceso a las acciones que componen nuestra cartera modelo y en vez del nombre de los valores verás la leyenda (pro).\n\nSi ya eres cliente de Kau Finanzas tendrás acceso libre a todas las acciones que componen nuestra cartera.\n\nLa rentabilidad de la cartera modelo Largos/cortos Europa se presenta con un apalancamiento del 1,8:1 y se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo nuestras cartera modelo largos/cortos Europa.',
				buttons:[
					{
						label: L('tutorial'),
						callback: function() {
							var web = Alloy.createController('web', 'http://vimeo.com/112182686').getView();
							web.open({left:0});
						}
					}
				]
			};
			break;
		case 'usa_long_short':
			var alert_data = {
				header:'Quantitative Value',
				title:'Long/Short América',
				text:'La cartera largos/cortos América se compone de un total de 16 valores.\n\n12 de ellos elegidos entre aquellos que nuestros radares han seleccionado automáticamente como los mejores valores americanos y 6 elegidos entre aquellos que nuestros radares han seleccionado como los peores.\n\nDe esta forma hemos creado una cartera de retorno absoluto basada en criterios de valor, con una composición 120% largos- 60% cortos, y una exposición neta alcista de un 60%\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el acceso a las acciones que componen nuestra cartera modelo y en vez del nombre de los valores verás la leyenda (pro).\n\nSi ya eres cliente de Kau Finanzas tendrás acceso libre a todas las acciones que componen nuestra cartera.\n\nLa rentabilidad de la cartera modelo Largos/cortos América se presenta con un apalancamiento del 1,8:1 y se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo nuestras cartera modelo largos/cortos América.',
				buttons:[
					{
						label: L('tutorial'),
						callback: function() {
							var web = Alloy.createController('web', 'http://vimeo.com/112182690').getView();
							web.open({left:0});
						}
					}
				]
			};
			break;
	}
} else {
	$.text.text = L('total_profitability');
	var alert_data = {
		header:'Quantitative Value',
		title:'Mi Cartera',
		text:'Crea tu propia cartera de Quantitative Value eligiendo los valores que más te gusten de los radares o copiando y mejorando nuestra cartera modelo.\n\nHaz un seguimiento de tus inversiones y de la rentabilidad que obtienes de forma sencilla y efectiva.\n\nLa rentabilidad de mi cartera se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial y  conoce más a fondo como sacar partido a mi cartera.',
		buttons:[
			/*{
				label: L('tutorial'),
				callback: function() {
					var web = Alloy.createController('web', 'http://www.kauapp.com').getView();
					web.open({left:0});
				}
			}*/
		]
	};
}
$.header.setHelp(alert_data);

var bbdd = require('bbdd');

if (args.from == 'qv_wallet') {
	$.header.setTitle(L('my_wallet') + ' QV');
} else {
	//$.header.setTitle(L(args.type));
	$.header.setTitle('QV investment Europa');
}

$.header.setWin($.win);

load();
function load() {
	new bbdd({
		type:args.type,
		from:args.from
	}, 'qvmodels/read', callback);
}

var total = 0;

function callback(data) {
	
	total = data.global_profit;
	data = data.data;
	
	if (data.length == 0) {
		var row = Ti.UI.createTableViewRow({
			height:'100dp',
			backgroundColor:'#F2F2F2'
		});
		row.add(Ti.UI.createLabel({
			width:'90%',
			text:L('no_data_wallet_qv'),
			color:'#666',
			textAlign:'center',
			font:{
				fontFamily:Alloy.CFG.fontFamily,
				fontSize:Alloy.CFG.fontSize
			}
		}));
		$.table.appendRow(row);
	}
	
	for (var i in data) {
		//total += parseFloat(data[i].profit);
		//total = data[i].global_profit;
		total += parseFloat(data[i].profit) / data.length;
		
		if (i >= 3 && args.from == 'model' && !Ti.App.Properties.getString('user_id', null) && !Ti.App.Properties.getInt('demo', true)) {
			data[i].cot = L('premium');
			//data[i].profit = L('premium');
			data[i].name = L('premium');
		}
		
		data[i].type = args.type;
		data[i]._from = args.from;

		var row = Alloy.createController('qv/row_wallet', data[i]).getView();
		row._data = data[i];
		$.table.appendRow(row);
	}
	
	if (total > 0) {
		$.percent.text = '+' + Alloy.Globals.number_format(total, 2, ',', '.') + '%';
		$.percent.color = Alloy.CFG.green;
	} else {
		$.percent.text = Alloy.Globals.number_format(total, 2, ',', '.') + '%';
		$.percent.color = Alloy.CFG.red;
	}
	
}

if (args.from == 'model') {
	//$.totals.remove($.reset);
	$.reset_img.image = '/images/finder.png';
	$.reset_text.text = L('historical');
	var place = 9;
} else {
	var place = 10;
}
if (Alloy.CFG.places[place].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = (parseInt(ads.height) + 80) + 'dp';
	$.totals.bottom = ads.height;
}

var confirm = Ti.UI.createAlertDialog({
	title:L('alert'),
	message:L('want_to_reset'),
	buttonNames:[L('yes'), L('no')],
	cancel:1
});
confirm.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		var rows = $.table.sections[0].rows;
		for (var i in rows) {
			$.table.deleteRow(rows[i]);
		}
		new bbdd({model:'Qv'}, 'wallets/reset', load);
	}
});

$.reset.addEventListener('singletap', function() {
	if (args.from == 'model') {
		showHistory();
	} else {
		confirm.show();
	}
});

if (args.from == 'model') {
	var buttons = [L('graph'), L('cancel')];
} else {
	var buttons = [L('graph'), L('delete'), L('cancel')];
}

var dialog = Ti.UI.createOptionDialog({
	title: L('want_to_do'),
	options: buttons,
	cancel: buttons.length - 1
});

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('view', {ticker: e.row._data.ticker, from: 'Qv'}).getView();
	win.open({left: 0});
	//dialog._e = e;
	//dialog.show();
});

dialog.addEventListener('click', function(e) {
	if (e.index === e.source.cancel) {
		return;
	}
	switch(e.index) {
		case 0:
			showGraph(this._e);
			break;
		case 1:
			remove(this._e);
			break;
	}
});

function remove(e) {
	if (args.from == 'model') {
		return;
	}
	var d = Ti.UI.createAlertDialog({
		title:L('remove_action'),
		message:String.format(L('remove_current_action'), e.row._data.name),
		buttonNames:[L('yes'), L('no')],
		cancel:1
	});
	d.show();
	d.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			new bbdd({
				ticker: e.row._data.ticker,
				model:'Qv'
			}, 'sts/add', function() {
				for (var i = $.table.sections[0].rows.length - 1; i >= 0; i --) {
					$.table.deleteRow(i);
				}
				load();
			});
		}
	});
}

function showGraph(e) {
	var graph = Alloy.createController('graph', {ticker: e.row._data.ticker, type: 'Qv'}).getView();
	$.win.add(graph);
	graph.addEventListener('singletap', function() {
		$.win.remove(graph);
	});
}

function showHistory() {
	var win = Alloy.createController('history', {type: args.type}).getView();
	win.open({left: 0});
}