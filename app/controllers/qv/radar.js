$.header.setWin($.win);
$.header.setTitle(L('radar'));

var bbdd = require('bbdd');

var alert_data = {
	header:'Quantitative Value',
	title:'Radares Cuantitativos',
	//text:'Nuestros radares seleccionan, basándose en los más estrictos ratios financieros, las mejores y las peores empresas en las que invertir en los mercados europeo y americano.\n\nLargos. En estos radares encontrarás las mejores compañías en las que invertir según su PER, su precio/ ventas, ROA ,etc.\n\nCortos. En estos radares encontrarás las peores compañías en las que invertir según su PER, su precio/ ventas, ROA, etc.\n\nTienes dos formas de usar nuestros radares:\n\nSi aun no eres cliente de Kau Finanzas puedes crear tu cartera con las compañías que muestran los radares de largos y mantener las acciones  durante un año sabiendo que están seleccionadas automáticamente con los mejores criterios cuantitativos. Puedes además utilizar los radares de cortos para realizar coberturas o estrategias long/short.\n\nTen en cuenta que  la operativa en corto conlleva riesgos y que todo lo que hagas lo harás bajo tu responsabilidad.\n\nSi ya eres cliente de Kau Finanzas sabrás seguir nuestros criterios (criterio de calidad) para saber qué y cuándo comprar y cómo gestionar tus carteras.\n\nLee la información de uso de cada radar para conocer más sobre él o accede a nuestro tutorial para conocer más a fondo nuestros radares.',
	text: 'Te presentamos los Radares Cuantitativos de KAU+, en ellos encontrarás toda la información que necesitas para crear sus carteras value o para conocer cuales son los mejores valores para operar haciendo trading según el estilo y las estrategias que  estés aprendiendo con tu coach.\n\nEncontrarás diversos radares tanto de cortos como de largos que se basan en los más estrictos ratios financieros, y que buscan las mejores y las peores empresas en las que invertir en los mercados europeo y americano.\n\nRadares Largos. En estos radares encontrarás las mejores compañías en las que invertir según su PER, su precio/ ventas, ROA ,etc.\nRadares Cortos. En estos radares encontrarás las peores compañías en las que invertir según su PER, su precio/ ventas, ROA, etc.',
	buttons:[
		/*{
			label: L('tutorial'),
			callback: function() {
				var web = Alloy.createController('web', 'http://www.kauapp.com').getView();
				web.open({left:0});
			}
		}*/
	]
};
$.header.setHelp(alert_data);

$.table.addEventListener('click', function(e) {
	//$.search.blur();
	var win = Alloy.createController('qv/finder', e.row.id).getView();
	win.open({left:0});
});

if (Alloy.CFG.places[6].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = ads.height;
}