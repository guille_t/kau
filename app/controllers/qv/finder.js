var args = arguments[0] || {};

var bbdd = require('bbdd');

$.header.setWin($.win);

var order = false;

$.reorder.addEventListener('singletap', function() {
	if (!Ti.App.Properties.getString('user_id', null) && !Ti.App.Properties.getInt('demo', true)) {
		Ti.UI.createAlertDialog({
			title:L('alert'),
			message:L('need_register'),
			ok:L('ok')
		}).show();
		return false;
	}
	$.select.animate({bottom: 0});
	// if (order) {
		// order = false;
		// $.reorder_text.text = L('reorder_ps');
	// } else {
		// order = true;
		// $.reorder_text.text = L('reorder_al');
	// }
});

if (args == 'eu_short' || args == 'usa_short') {
	$.value.opacity = $.quality.opacity = 0.5;
} else {
	$.value.addEventListener('singletap', select);
	$.quality.addEventListener('singletap', select);
}

$.alphabetical.addEventListener('singletap', select);
$.momentum.addEventListener('singletap', select);

function select(e) {
	var rows = $.table.sections[0].rows;
	for (var i in rows) {
		$.table.deleteRow(rows[i]);
	}
	$.reorder_text.text = e.source.text;
	new bbdd({type:args, new_order:e.source.id}, 'qvs/read', callback);
	$.select.animate({bottom: '-200dp'});
}

switch (args) {
	case 'eu_long':
		var alert_data = {
			header:'Quantitative Value',
			title:'Radar Europa Largos',
			text:'La compañías que ha seleccionado nuestro radar europeo de largos son las mejores empresas europeas según sus estados financieros.\n\nTodas ellas han pasado con éxito los más estrictos filtros cuantitativos  de tal forma que representan una selección de las compañías que mejores ratios financieros presentan en el mercado europeo.\n\nPuedes utilizar este radar de dos manera diferentes:\n\nSi aun no eres cliente de Kau Finanzas  puedes crear con él tu cartera y mantener las acciones que compres durante un año sabiendo que son las que presentan actualmente mejores ratios cuantitativos.\n\nSi ya eres cliente de Kau Finanzas sabrás seguir nuestro  criterio de calidad y, por lo tanto, sabrás perfectamente cuándo invertir en cada una de ellas y cuándo debes vender algún valor y comprar otro tal y como sucede en la cartera modelo.\n\nAñade cualquiera de ellas a tu cartera haciendo click en el icono mi cartera.\n\nAccede a nuestro tutorial para conocer más a fondo nuestro radar y nuestro criterio de calidad.',
			buttons:[
				{
					label: L('tutorial'),
					callback: function() {
						var web = Alloy.createController('web', 'http://vimeo.com/112188787').getView();
						web.open({left:0});
					}
				}
			]
		};
		$.header.setTitle(L('eu'));
		break;
	case 'eu_short':
		var alert_data = {
			header:'Quantitative Value',
			title:'Radar Europa Cortos',
			text:'La compañías que ha seleccionado nuestro radar europeo de cortos son las peores empresas europeas según sus estados financieros.\n\nTodas ellas han pasado con suspenso los más estrictos filtros cuantitativos de tal forma que representan una selección de las compañías que peores ratios financieros presentan en el mercado europeo.\n\nPuedes utilizar este radar de dos manera diferentes:\n\nSi aun no eres cliente de Kau Finanzas puedes crear con el tus coberturas o carteras long/short sabiendo que son las que presentan actualmente peores ratios cuantitativos.\n\nTen en cuenta que la operativa en corto conlleva riesgos y que lo que hagas lo harás bajo tu responsabilidad.\n\nPuedes contactar con Kau Finanzas desde “saber mas” para conocer las posibilidades de la operativa en corto.\n\nSi ya eres cliente de Kau Finanzas sabrás seguir nuestro  criterio de calidad  y, por lo tanto, sabrás perfectamente cómo utilizar nuestros radares de cortos de forma segura y efectiva.\n\nAñade cualquiera de ellas a tu cartera haciendo click en el icono mi cartera.\n\nAccede a nuestro tutorial para conocer más a fondo nuestro radar, el criterio de calidad y las posibilidades que tienes para realizar completas y sofisticadas estrategias long/short.',
			buttons:[
				{
					label: L('tutorial'),
					callback: function() {
						var web = Alloy.createController('web', 'http://vimeo.com/112195808').getView();
						web.open({left:0});
					}
				}
			]
		};
		$.header.setTitle(L('eu'));
		break;
	case 'usa_long':
		var alert_data = {
			header:'Quantitative Value',
			title:'Radar América Largos',
			text:'La compañías que ha seleccionado nuestro radar americano de largos son las mejores empresas americanas según sus estados financieros.Todas ellas han pasado con éxito los más estrictos filtros cuantitativos  de tal forma que representan una selección de las compañías que mejores ratios financieros presentan en el mercado americano.\n\nPuedes utilizar este radar de dos manera diferentes:\n\nSi aun no eres cliente de Kau Finanzas puedes crear con él tu cartera y mantener las acciones  que compres durante un año sabiendo que son las que presentan actualmente mejores ratios cuantitativos.\n\nSi ya eres cliente de Kau Finanzas sabrás seguir nuestro  criterio de calidad y por lo tanto, sabrás perfectamente cuándo invertir en cada una de ellas y cuándo debes vender algún valor y comprar otro tal y como sucede en la cartera modelo.\n\nAñade cualquiera de ellas a tu cartera haciendo click en el icono mi cartera.\n\nAccede a nuestro tutorial para conocer más a fondo nuestro radar y nuestro criterio de calidad.',
			buttons:[
				{
					label: L('tutorial'),
					callback: function() {
						var web = Alloy.createController('web', 'http://vimeo.com/112148310').getView();
						web.open({left:0});
					}
				}
			]
		};
		$.header.setTitle(L('usa'));
		break;
	case 'usa_short':
		var alert_data = {
			header:'Quantitative Value',
			title:'Radar América Cortos',
			text:'La compañías que ha seleccionado nuestro radar americano de cortos son las peores empresas americanas según sus estados financieros.\n\nTodas ellas han pasado con suspenso los más estrictos filtros cuantitativos de tal forma que representan una selección de las compañías que peores ratios financieros presentan en el mercado americano.\n\nPuedes utilizar este radar de dos manera diferentes:\n\nSi aun no eres cliente de Kau Finanzas puedes crear con el tus coberturas o carteras long/short sabiendo que son las que presentan actualmente peores ratios cuantitativos. \n\nTen en cuenta que la operativa en corto conlleva riesgos y que lo que hagas lo harás bajo tu responsabilidad.\n\nPuedes contactar con Kau Finanzas desde “saber mas” para conocer las posibilidades de la operativa en corto.\n\nsi ya eres cliente de Kau Finanzas sabrás seguir nuestro  criterio de calidad  y, por lo tanto, sabrás perfectamente cómo utilizar nuestros radares de cortos de forma segura y efectiva.\n\nAñade cualquiera de ellas a tu cartera haciendo click en el icono mi cartera.\n\nAccede a nuestro tutorial para conocer más a fondo nuestro radar, nuestro criterio de calidad y las posibilidades que te ofrecen  nuestros radares para realizar completas y sofisticadas estrategias long/short.',
			buttons:[
				{
					label: L('tutorial'),
					callback: function() {
						var web = Alloy.createController('web', 'http://vimeo.com/112192086').getView();
						web.open({left:0});
					}
				}
			]
		};
		$.header.setTitle(L('usa'));
		break;
}
if (alert_data) {
	$.header.setHelp(alert_data);
}

var global_profit = 0;

new bbdd({type:'qv_wallet', from: 'my'}, 'qvmodels/read', function(data) {
	for (var i = 0; i < data.data.length; i ++) {
		if (parseInt(data.data[i].profit) != 0) {
			global_profit = 1;
		}
	}
	new bbdd({type:args}, 'qvs/read', callback);
});

function callback(data) {
	for (var i in data) {
		var d = data[i].Qv;
		d.global_profit = global_profit;
		var rowView = Alloy.createController('qv/row', data[i].Qv);
		row = rowView.getView();
		row._data = data[i].Qv;
		row._callback = rowView.adding;
		$.table.appendRow(row);
	}
}

if (!Ti.App.Properties.getString('user_id', null)) {
	//$.win.remove($.reorder);
	//$.table.bottom = 0;
}

if (Alloy.CFG.places[7].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = parseInt($.table.bottom) + parseInt(ads.height) + 'dp';
	$.reorder.bottom = ads.height;
}

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('view', {ticker: e.row._data.ticker, from: 'qv'}).getView();
	win.open({left: 0});
	return;
	if (e.row._data.haveIt) {
		var buttons = [L('graph'), L('remove'), L('cancel')];
	} else {
		var buttons = [L('graph'), L('add'), L('cancel')];
	}
	
	var dialog = Ti.UI.createOptionDialog({
		title: L('want_to_do'),
		options: buttons,
		cancel: buttons.length - 1
	});
	dialog._e = e;
	dialog.show();

	dialog.addEventListener('click', function(e) {
		if (e.index === e.source.cancel) {
			return;
		}
		switch(e.index) {
			case 0:
				showGraph(this._e);
				break;
			case 1:
				this._e.row._callback();
				break;
		}
	});
});

function showGraph(e) {
	var graph = Alloy.createController('graph', {ticker: e.row._data.ticker, type: 'Qv'}).getView();
	$.win.add(graph);
	graph.addEventListener('singletap', function() {
		$.win.remove(graph);
	});
}