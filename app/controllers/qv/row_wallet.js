var args = arguments[0] || {};

if (args.type && (args.type == 'usa_long_short' || args.type == 'eu_long_short')) {
	switch (args.position) {
		case 'usa_short':
		case 'eu_short':
			$.position.text = 'Corto';
			$.position.color = Alloy.CFG.red;
			break;
		case 'usa_long':
		case 'eu_long':
			$.position.text = 'Largo';
			$.position.color = Alloy.CFG.green;
			break;
	}
	$.quote.left = '44%';
}

$.name.text = args.name;

if (args.cot == L('premium')) {
	$.quote.text = args.cot;
} else {
	$.quote.text = Alloy.Globals.number_format(args.cot, 2, ',', '.');
}

if (args.profit == L('premium')) {
	$.profit.text = args.profit;
} else {
	if (args.profit > 0) {
		$.profit.text = '+' + Alloy.Globals.number_format(args.profit, 2, ',', '.') + '%';
		$.profit.color = Alloy.CFG.green;
	} else if (args.profit == 0) {
		$.profit.text = 0;
	} else {
		$.profit.text = Alloy.Globals.number_format(args.profit, 2, ',', '.') + '%';
		$.profit.color = Alloy.CFG.red;
	}
}

if (args._from == 'model') {
	$.date.text = args.date;
}
