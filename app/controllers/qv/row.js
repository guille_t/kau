var args = arguments[0] || {};

var bbdd = require('bbdd');

$.name.text = args.title;
$.ticker.text = args.ticker;
//$.cap.text = Alloy.Globals.number_format(args.cap, 2, ',', '.');
//$.cap.text = args.cap.replace('.', ',');
$.cap.text = (Math.round(args.latest * 100) / 100).toString().replace('.', ',');

// if (args.haveIt) {
	// $.add_image.image = '/images/remove.png';
// } else {
	// $.add_image.image = '/images/add.png';
// }

var d = Ti.UI.createAlertDialog({
	title:L('alert'),
	message:L('reset_wallet'),
	buttonNames:[L('yes'), L('no')],
	cancel:1
});

d.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		adding();
	}
});

//$.add.addEventListener('singletap', function() {
exports.adding = function() {
	if (args.global_profit != 0) {
		d.show();
	} else {
		adding();
	}
};
//});

function callback(data) {
	//alert(data);
}

function adding() {
	// if ($.add_image.image == '/images/add.png') {
		// $.add_image.image = '/images/remove.png';
		// $.row._data.haveIt = true;
	// } else {
		// $.add_image.image = '/images/add.png';
		// $.row._data.haveIt = false;
	// }
	
	if ($.row._data.haveIt == false) {
		$.row._data.haveIt = true;
	} else {
		$.row._data.haveIt = false;
	}
	
	new bbdd({
		ticker:args.ticker,
		model:'Qv'
	}, 'sts/add');
}
