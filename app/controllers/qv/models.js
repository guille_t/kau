$.header.setWin($.win);
$.header.setTitle(L('model_wallets'));

var alert_data = {
	header:'Quantitative Value',
	title:'Carteras Modelo',
	text:'Descubre aquí nuestras carteras modelo Quantitative Value. Estas carteras representan un ejemplo práctico de la aplicación del Quantitaive Value y  están creadas de forma totalmente automática con la información de nuestros radares y el orden ofrecido por nuestro criterio de calidad.\n\nPuedes seguir la rentabilidad de nuestras carteras y estudiarlas para copiarlas y mejorarlas en tus inversiones.\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el acceso a las acciones que componen nuestras carteras modelo y en vez del nombre de los valores verás la leyenda (pro).\n\nSi ya eres cliente de Kau Finanzas tendrás acceso libre a todas las acciones que componen nuestra carteras.\n\nLa rentabilidad de todas las carteras modelo se presenta con apalancamiento se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer mas a fondo nuestras carteras modelo.',
	buttons:[
		{
			label: L('tutorial'),
			callback: function() {
				var web = Alloy.createController('web', 'http://vimeo.com/112121650').getView();
				web.open({left:0});
			}
		}
	]
};
$.header.setHelp(alert_data);

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('qv/wallet', {type:e.row.id, from:'model'}).getView();
	win.open({left:0});
});

if (Alloy.CFG.places[8].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = ads.height;
}