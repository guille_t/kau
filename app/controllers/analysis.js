var bbdd = require('bbdd');
var args = arguments[0] || {};

if (args.premium == false) {
	var alert_data = {
		header:'Acceso bloqueado',
		title:'Cuenta premium',
		text:Alloy.CFG.premium_message,
		buttons:[{
			label: L('close'),
			callback: function() {
				not.animate({opacity:0}, function() {
					$.win.remove(not);
				});
			}
		}]
	};
	var not = Alloy.createController('alerts', alert_data).getView();
	$.win.add(not);
	not.animate({opacity:1});
}

$.header.setWin($.win);
$.header.setTitle(args.St.title);

var share = Ti.UI.createView({
	width: '20%',
	right: 0
});
share.add(Ti.UI.createImageView({
	image: '/images/share.png',
	width: '30%'
}));
$.header.addItem(share);
share.addEventListener('singletap', function() {
	//sharerDialog.show();
	var socialWidget = Alloy.createWidget('com.alcoapps.socialshare');
	socialWidget.share({
	    status              : 'Analisis de ' + shareTitle + ' http://www.kaufinanzas.es/analisis/?ticker=' + args.St.ticker,
	    androidDialogTitle  : L('sharing')
	});
});

$.loading.show();

var profit = (args.St.cot / args.St.close * 100 - 100).toFixed(2);

if (profit > 0) profit = '+' + profit;
$.name.text = args.St.title + ' (' + args.St.ticker + ')';
$.data.text = args.St.cot + ' (' + profit + '%)';

args.temp = '[TEMP]';

var lipsum = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

if (args.Qv) {
	var isQV = 'cumple';
	switch (args.Qv.type) {
		case 'eu_long': var unit = '€'; var reduced = 0; var pos = 'europeo de largos'; break;
		case 'usa_long': var unit = '$'; var reduced = 0; var pos = 'americano de largos'; break;
		case 'eu_short': var unit = '€'; var reduced = 1; var pos = 'europeo de cortos'; break;
		case 'usa_short': var unit = '$'; var reduced = 1; var pos = 'americano de cortos'; break;
		case 'activist': var unit = '€'; var reduced = 2; var pos = 'activist style'; break;
		case 'ev': var unit = '€'; var reduced = 3; var pos = 'activist style'; break;
		case 'all': var unit = '€'; var reduced = 4; var pos = ''; break;
	}
	if (pos) {
		var radar = 'encontrándose en el radar ' + pos;
	} else {
		var isQV = 'no cumple';
		var radar = '';
	}
} else {
	var isQV = 'no cumple';
	var radar = '';
}

if (args.index) {
	$.strategy.text = args.St.title + ' tendencia ' + (args.St.position == 'long' ? 'alcista' : 'bajista') + ' de corto plazo. Su índice de referencia, el ' + args.index.St.title + 
		', presenta una tendencia ' + (args.index.St.position == 'long' ? 'alcista' : 'bajista') + ' de corto plazo.\n\nEl valor ' + isQV + ' los criterios del Quantitative Value ' + radar;
} else {
	$.strategy.text = args.St.title + ' tendencia ' + (args.St.position == 'long' ? 'alcista' : 'bajista') + ' de corto plazo.\n\nEl valor ' + isQV + ' los criterios del Quantitative Value ' + radar;
}

// $.st_text.text = 'El Swing Trading es una técnica de inversión que utiliza los gráficos que la cotización de las acciones dibuja sesión a sesión para detectar las tendencias, ya sean alcistas o bajistas, y seguir al mercado, aprovechándolas para ganar dinero, tanto cuando el mercado sube como cuando cae.\n\nPara aplicar la estrategia se puede usar cualquier producto financiero: acciones, futuros o ETF. Si bien, para implementar esta técnica, un producto que consideramos versátil y fácil de usar es el CFD.';

$.st_explain.text = 'Mantendríamos el valor en cartera con expectativas ' + (args.St.position == 'long' ? 'alcistas' : 'bajistas') + ' mientras no ' + (args.St.position == 'long' ? 'pierda' : 'supere') + 
	' hoy los ' + (args.St.position == 'long' ? parseFloat(args.St.stop_long).toFixed(2) : parseFloat(args.St.stop_short).toFixed(2)) + unit + '.\n\nEl máximo previsto para la sesión de hoy es de ' + parseFloat(args.St.max_prev).toFixed(2) + unit + ' y el mínimo previsto de '
	 + parseFloat(args.St.min_prev).toFixed(2) + unit + '. El valor podría moverse entre los valores comprendidos en ese intervalo sin que ello implique un cambio en la tendencia vigente.';

$.graph.image = 'https://chart.finance.yahoo.com/z?s=' + args.St.ticker + '&t=1y&q=&l=&z=l&a=v&p=s&lang=es-ES&region=ES&v=' + Math.round(Math.random() * 1000);
Ti.API.info($.graph.image);

$.st_explain2.text = 'En caso de que el valor rompa el stop loss de la sesión de hoy podríamos ' + (args.St.position == 'long' ? 'vender' : 'comprar') + ' el valor o abrir expectativas ' + (args.St.position == 'long' ? 'bajistas por debajo' : 'alcistas por encima') + ' de ' + (args.St.position == 'long' ? parseFloat(args.St.stop_long).toFixed(2) : parseFloat(args.St.stop_short).toFixed(2)) + unit + '.';

if (args.index) {
	$.index_text.text = 'El valor toma como referencia el índice ' + args.index.St.title + '.\n\nEl ' + args.index.St.title + ' mantiene expectativas ' + (args.index.St.position == 'long' ? 'alcistas' : 'bajistas') + ' mientras no ' + (args.index.St.position == 'long' ? 'pierda' : 'supere') + ' los ' + (args.St.position == 'long' ? parseFloat(args.index.St.stop_long).toFixed(2) : parseFloat(args.index.St.stop_short).toFixed(2)) + unit + '.';
} else {
	$.scrollview.remove($.index_text);
	$.scrollview.remove($.index_block);
	$.scrollview.remove($.qv_block);
}

// $.qv_text.text = 'El Quantitative Value es una evolución del Value Investment que nos permite, a través de un análisis puramente cuantitativo y, usando las nuevas tecnologías, seleccionar las mejores empresas en las que invertir en Bolsa, empresas que tienen unos ratios financieros excelentes a bajo precio.\n\nEl Quantitative Value utiliza 11 ratios para seleccionar las mejores empresas en las que invertir. Éstos ratios son fruto del estudio de los criterios de inversión de Joel Greenblatt, Benjamin Graham, Joseph Piotroski y Cliff Asness.';

$.qv_explain.text = args.text;

if (args.Qv) {
	if (reduced == 1) { // Cortos
		$.hide_qv.remove($.p1);
		$.hide_qv.remove($.p3);
		$.hide_qv.remove($.p4);
		$.hide_qv.remove($.p5);
		$.hide_qv.remove($.p7);
		$.hide_qv.remove($.p8);
		$.hide_qv.remove($.remove_me);
		$.hide_qv.remove($.remove_me2);
		$.hide_qv.remove($.p7);
		
		$.p2.text = '1) Current Ratio:\n' + parseFloat(args.Qv.gross).toFixed(2) + '\nLa Razón Corriente mide el nivel de liquidez de una empresa y se calcula con esta formula: Capital de trabajo/Pasivos corrientes. Una empresa en dificultades para hacer frente a los pagos de corto plazo tendrá una Razón Corriente inferior a dos. KAU+ busca eso, empresas con una razón corriente baja para posiciones cortas.';
		$.p6.text = '2) P/S:\n' + parseFloat(args.Qv.ps).toFixed(2) + '\nLa relación precio/ventas es preferible que sea alta si queremos invertir en una empresa con potencial bajista. Interesa vender negocios que reflejen en el precio de la acción varias veces las ventas de la empresa.';
		$.p9.text = '3) ROI:\n' + parseFloat(args.Qv.roi).toFixed(2) + '%\nKAU+ busca bajos retornos sobre la inversión.';
		$.p10.text = '4) ROE:\n' + parseFloat(args.Qv.roe).toFixed(2) + '%';
		$.p11.text = '5) ROA:\n' + parseFloat(args.Qv.roa).toFixed(2) + '%\nKAU+ busca ROA y ROE bajos sin ser necesario que el ROE sea mayor que el ROA, pero si dando importancia a que exista una diferencia absoluta entre ambos reducida.\n\nEsa reducida diferencia nos indica que el mix de eficiencia financiera de la compañía esta deteriorado ya que el coste medio de la deuda podría ser superior a la rentabilidad económica.';
	} else if (reduced == 0) { // Largos
		$.p1.text = '1) Market Cap (GBP):\n' + args.Qv.cap + '\nKAU+ prefiere empresas que no sean micro-caps ni small-cap';
		$.p2.text = '2) Gross Margin:\n' + parseFloat(args.Qv.gross).toFixed(2) + '%\nKAU+ busca márgenes brutos elevados.';
		$.p3.text = '3) Net Profit Margin:\n' + parseFloat(args.Qv.net_profit).toFixed(2) + '%\nKAU+ busca márgenes netos positivos y preferiblemente elevados.';
		$.p4.text = '4) P/BV:\n' + parseFloat(args.Qv.pbv).toFixed(2) + '\nKAU+ busca relaciones entre precio y valor en los libros no muy elevadas, ya que lo contrario indicaría cierta "burbuja" en el precio de la acción.';
		$.p5.text = '5) PER:\n' + parseFloat(args.Qv.per).toFixed(2) + '\nKAU+ busca PERs inferiores a la media USA (15) pero tampoco demasiado bajos.';
		$.p6.text = '6) P/S:\n' + parseFloat(args.Qv.ps).toFixed(2) + '\nKAU+ busca una relación baja entre precio y ventas. No interesa comprar negocios que reflejen varias veces las ventas en el precio de la acción.';
		$.p7.text = '7) Quick Ratio:\n' + parseFloat(args.Qv.quick_ratio).toFixed(2) + '\nKAU+ busca valores que tengan este ratio en el entorno de 1. Un Quick Ratio menor que uno indicaría un pasivo circulante excesivamente alto en relación al activo y seria aconsejable vender existencias para poder hacer frente mejor a las deudas a corto plazo.';
		$.p8.text = '8) Debt to capital:\n' + parseFloat(args.Qv.debt).toFixed(2) + '\nLa deuda sobre el capital determina el nivel de endeudamiento de la empresa. KAU+ busca empresas con un bajo nivel de endeudamiento.';
		$.p9.text = '9) ROI:\n' + parseFloat(args.Qv.roi).toFixed(2) + '%\nKAU+ busca retornos sobre la inversión positivos y elevados.';
		$.p10.text = '10) ROE:\n' + parseFloat(args.Qv.roe).toFixed(2) + '%';
		$.p11.text = '11) ROA:\n' + parseFloat(args.Qv.roa).toFixed(2) + '%\nKAU+ busca ROA y ROE positivos y elevados además es necesario que el ROE sea mayor que el ROA y que exista una amplia diferencia entre ambos.\n\nEsa amplia diferencia entre ambos nos indica que el coste medio de la deuda es inferior a la rentabilidad económica y por lo tanto la compañía presenta un buen mix de eficiencia financiera.';
	} else if  (reduced == 2){ // Activist
		$.hide_qv.remove($.p1);
		$.hide_qv.remove($.p4);
		$.hide_qv.remove($.p5);
		$.hide_qv.remove($.p8);
		$.hide_qv.remove($.remove_me2);
		$.p2.text = '1) Gross Margin:\n' + parseFloat(args.Qv.gross).toFixed(2) + '%\nKAU+ busca márgenes brutos elevados.';
		$.p3.text = '2) Net Profit Margin:\n' + parseFloat(args.Qv.net_profit).toFixed(2) + '%\nKAU+ busca márgenes netos positivos y preferiblemente elevados.';
		$.p6.text = '3) P/S:\n' + parseFloat(args.Qv.ps).toFixed(2) + '\nKAU+ busca una relación baja entre precio y ventas. No interesa comprar negocios que reflejen varias veces las ventas en el precio de la acción.';
		$.p7.text = '4) Quick Ratio:\n' + parseFloat(args.Qv.quick_ratio).toFixed(2) + '\nKAU+ prefiere, para acciones de tipo Activist, empresas con un QR elevado ya que las partidas de tesorería, las inversiones financieras a corto plazo y los deudores, en relación al pasivo corriente, serán muy elevadas indicando que la distancia a la liquidez es muy corta y que son grandes generadoras de caja.';
		$.p9.text = '5) ROI:\n' + parseFloat(args.Qv.roi).toFixed(2) + '%\nKAU+ busca retornos sobre la inversión positivos y elevados.';
		$.p10.text = '6) ROE:\n' + parseFloat(args.Qv.roe).toFixed(2) + '%';
		$.p11.text = '7) ROA:\n' + parseFloat(args.Qv.roa).toFixed(2) + '%\nKAU+ busca ROA y ROE positivos y elevados además es necesario que el ROE sea mayor que el ROA y que exista una amplia diferencia entre ambos.\n\nEsa amplia diferencia entre ambos nos indica que el coste medio de la deuda es inferior a la rentabilidad económica y por lo tanto la compañía presenta un buen mix de eficiencia financiera.';
	} else if (reduced == 3) { // EV
		$.p1.text = '1) Market Cap (GBP):\n' + args.Qv.cap + '\nKAU+ prefiere, para acciones de tipo Activist, empresas que sean small-cap.';
		$.p2.text = '2) Gross Margin:\n' + parseFloat(args.Qv.gross).toFixed(2) + '%\nKAU+ busca márgenes brutos elevados.';
		$.p3.text = '3) Net Profit Margin:\n' + parseFloat(args.Qv.net_profit).toFixed(2) + '%\nKAU+ busca márgenes netos positivos y preferiblemente elevados.';
		$.p5.text = '4) PER:\n' + parseFloat(args.Qv.per).toFixed(2) + '\nKAU+ busca PERs inferiores a la media USA (15) pero tampoco demasiado bajos.';
		$.p6.text = '5) P/S:\n' + parseFloat(args.Qv.ps).toFixed(2) + '\nKAU+ busca una relación baja entre precio y ventas. No interesa comprar negocios que reflejen varias veces las ventas en el precio de la acción.';
		$.p7.text = '6) Quick Ratio:\n' + parseFloat(args.Qv.quick_ratio).toFixed(2) + '\nKAU+ busca valores que tengan este ratio en el entorno de 1. Un Quick Ratio menor que uno indicaría un pasivo circulante excesivamente alto en relación al activo y seria aconsejable vender existencias para poder hacer frente mejor a las deudas a corto plazo.';
		$.p8.text = '7) Debt to capital:\n' + parseFloat(args.Qv.debt).toFixed(2) + '\nLa deuda sobre el capital determina el nivel de endeudamiento de la empresa. KAU+ busca empresas con un bajo nivel de endeudamiento.';
		$.p9.text = '8) ROI:\n' + parseFloat(args.Qv.roi).toFixed(2) + '%\nKAU+ busca retornos sobre la inversión positivos y elevados.';
		$.p10.text = '9) ROE:\n' + parseFloat(args.Qv.roe).toFixed(2) + '%';
		$.p11.text = '10) ROA:\n' + parseFloat(args.Qv.roa).toFixed(2) + '%\nKAU+ busca ROA y ROE positivos y elevados además es necesario que el ROE sea mayor que el ROA y que exista una amplia diferencia entre ambos.\n\nEsa amplia diferencia entre ambos nos indica que el coste medio de la deuda es inferior a la rentabilidad económica y por lo tanto la compañía presenta un buen mix de eficiencia financiera.';
	} else if (reduced == 4) { // Todos
		$.p1.text = '1) Market Cap (GBP):\n' + args.Qv.cap + '\nKAU+ prefiere, para acciones de tipo Activist, empresas que sean small-cap.';
		$.p2.text = '2) Gross Margin:\n' + (parseFloat(args.Qv.gross).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.gross).toFixed(2)) + '%\nKAU+ busca márgenes brutos elevados.';
		$.p3.text = '3) Net Profit Margin:\n' + (parseFloat(args.Qv.net_profit).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.net_profit).toFixed(2)) + '%\nKAU+ busca márgenes netos positivos y preferiblemente elevados.';
		$.p4.text = '4) P/BV:\n' + (parseFloat(args.Qv.pbv).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.pbv).toFixed(2)) + '\nKAU+ busca relaciones entre precio y valor en los libros no muy elevadas, ya que lo contrario indicaría cierta "burbuja" en el precio de la acción.';
		$.p5.text = '4) PER:\n' + (parseFloat(args.Qv.per).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.per).toFixed(2)) + '\nKAU+ busca PERs inferiores a la media USA (15) pero tampoco demasiado bajos.';
		$.p6.text = '5) P/S:\n' + (parseFloat(args.Qv.ps).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.ps).toFixed(2)) + '\nKAU+ busca una relación baja entre precio y ventas. No interesa comprar negocios que reflejen varias veces las ventas en el precio de la acción.';
		$.p7.text = '6) Quick Ratio:\n' + (parseFloat(args.Qv.quick_ratio).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.quick_ratio).toFixed(2)) + '\nKAU+ busca valores que tengan este ratio en el entorno de 1. Un Quick Ratio menor que uno indicaría un pasivo circulante excesivamente alto en relación al activo y seria aconsejable vender existencias para poder hacer frente mejor a las deudas a corto plazo.';
		$.p8.text = '7) Debt to capital:\n' + (parseFloat(args.Qv.debt).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.debt).toFixed(2)) + '\nLa deuda sobre el capital determina el nivel de endeudamiento de la empresa. KAU+ busca empresas con un bajo nivel de endeudamiento.';
		$.p9.text = '8) ROI:\n' + (parseFloat(args.Qv.roi).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.roi).toFixed(2)) + '%\nKAU+ busca retornos sobre la inversión positivos y elevados.';
		$.p10.text = '9) ROE:\n' + (parseFloat(args.Qv.roe).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.roe).toFixed(2)) + '%';
		$.p11.text = '10) ROA:\n' + (parseFloat(args.Qv.roa).toFixed(2) == 0 ? '--' : parseFloat(args.Qv.roa).toFixed(2)) + '%\nKAU+ busca ROA y ROE positivos y elevados además es necesario que el ROE sea mayor que el ROA y que exista una amplia diferencia entre ambos.\n\nEsa amplia diferencia entre ambos nos indica que el coste medio de la deuda es inferior a la rentabilidad económica y por lo tanto la compañía presenta un buen mix de eficiencia financiera.';
	}
} else {
	$.scrollview.remove($.hide_qv);
}

if (args.index) {
	var firsts = [];
	new bbdd({type: 'markets'}, 'thermometers/get', function(data) {
		switch (args.index.St.ticker) {
			case '^GDAXI': var current_country = 'Alemania'; break;
	        case '^IBEX': var current_country = 'España'; break;
	        case 'PSI20.LS': var current_country = 'Portugal'; break;
	        case 'FTSEMIB.MI': var current_country = 'Italia'; break;
	        case '^FTSE': var current_country = 'UK'; break;
	        case '^FCHI': var current_country = 'Francia'; break;
	        default: var current_country = 'EEUU'; break;
		}
		var j = 0;
		for (var i in data) {
			if (j <= 2) {
				firsts.push(data[i].title);
			}
			j ++;
			if (current_country == data[i].title) {
				var current_value = data[i].percent;
			}
		}
		var last = firsts.pop();
		$.distribution_text.text = 'En relación a la distribución geográfica recomendada, cabe destacar que ' + current_country + 
			' no debería superar el ' + current_value + '% del peso de una cartera fundamental que tenga vocación global, por existir otros mercados a su vez interesantes en términos fundamentales.' +
			'\n\nSon ' + firsts.join(', ') + ' y ' + last + ' los países que deberían estar más ponderados.';
	});
} else {
	$.scrollview.remove($.distribution_block);
	$.scrollview.remove($.distribution_text);
}

$.warning.text = 'Se aconseja revisar semanalmente, como mínimo, el informe de análisis por si las concurrencias de mercado modificaran la visión actual y poder así ajustar las expectativas sobre el valor en caso de que estas cambien.';

setTimeout(function() {
	$.loading.hide();
}, 1000);
$.scrollview.show();

// Sharing
var shareTitle = args.St.title;
var shareText = 'Analisis de ' + shareTitle;
var shareURL = 'http://www.kaufinanzas.es/analisis/?ticker=' + args.St.ticker;
var sharerDialog = Ti.UI.createOptionDialog({
	options: ['Email', 'Facebook', 'Twitter', L('cancel')],
	cancel: 3
});
sharerDialog.addEventListener('click', function(e) {
	switch (e.index) {
		case 0:
			var emailDialog = Titanium.UI.createEmailDialog();
			emailDialog.subject = shareTitle;
			emailDialog.html = true;
			emailDialog.toRecipients = null;
			emailDialog.messageBody = shareText + '\n\n' + shareURL;
			emailDialog.open();
			break;
		case 1:
			shareFacebook();
			break;
		case 2:
			shareTwitter();
			break;
	}
});

function shareFacebook() {
	if (OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isFacebookSupported()){ //min iOS6 required
			Social.facebook({
				text:shareText,
				url:shareURL
			});
	    } else {
			oldMode('f');
	    }
	} else {
		oldMode('f');
	}
}

function shareTwitter() {
	if (OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isTwitterSupported()){ //min iOS5 required
			Social.twitter({
				text:shareText,
				url:shareURL
			});
		} else {
			oldMode('t');
		}
	} else {
		oldMode('t');
	}
}

function oldMode(type) {
	if (OS_ANDROID) {
		var intent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_SEND,
			type : "text/plain"
		});
	 
		intent.putExtra(Ti.Android.EXTRA_TEXT, shareText);
		intent.addCategory(Ti.Android.CATEGORY_DEFAULT);
		Ti.Android.currentActivity.startActivity(intent);
	} else {			
		var win = Ti.UI.createWindow({backgroundColor:'#333'});
		if (type == 't') {
			win.add(Ti.UI.createWebView({top:'50dp', url:'http://twitter.com/share?text=' + shareText + ' ' + shareURL}));
		} else {
			win.add(Ti.UI.createWebView({top:'50dp', url:'http://facebook.com/sharer.php?u=' + shareURL}));
		}
		var close = Ti.UI.createButton({title:'Cerrar', left:'10dp', top:'15dp', backgroundImage:'none', color:'#FFF'});
		win.add(close);
		win.open();
		close.addEventListener('click', function() {
			win.close();
		});
	}
}