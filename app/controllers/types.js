var args = arguments[0] || {};

var newArgs = {};

switch (args) {
	case 'model':
		$.header.setTitle(L('strategies'));
		$.section_title.text = L('strategies');
		var file_st = 'swing/wallet';
		var file_qv = 'qv/wallet';
		newArgs.type = 'eu_long';
		newArgs.from = 'model';
		break;
	case 'wallet':
		$.header.setTitle(L('my_wallet'));
		$.section_title.text = L('my_wallet');
		var file_st = 'swing/wallet';
		var file_qv = 'qv/wallet';
		newArgs.from = 'qv_wallet';
		//newArgs.type = 'wallet';
		break;
}

$.header.setWin($.win);

$.eu_long.addEventListener('click', function(e) { // Swing
	var win = Alloy.createController(file_st, args).getView();
	win.open({left: 0});
});
$.usa_long.addEventListener('click', function(e) { // QV
	var win = Alloy.createController(file_qv, newArgs).getView();
	win.open({left: 0});
});
