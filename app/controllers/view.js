var args = arguments[0] || {};

$.header.setWin($.win);

var haveIt = false;

var data = null;

$.loading.show();

var bbdd = require('bbdd');
new bbdd({ticker: args.ticker, model: args.from}, 'sts/get_info', function(d) {
	
	data = d;
	
	if (data.premium == false) {
		var alert_data = {
			header:'Acceso bloqueado',
			title:'Cuenta premium',
			text:Alloy.CFG.premium_message,
			buttons:[{
				label: L('close'),
				callback: function() {
					not.animate({opacity:0}, function() {
						$.win.remove(not);
					});
				}
			}]
		};
		var not = Alloy.createController('alerts', alert_data).getView();
		$.win.add(not);
		not.animate({opacity:1});
	}
	
	haveIt = data.haveIt;
	
	if (!haveIt) {
		$.add.title = L('add');
	} else {
		$.add.title = L('remove_s');
	}
	
	$.header.setTitle(data.St.title);
	
	var profit = (data.St.cot / data.St.close * 100 - 100).toFixed(2);
	
	if (profit > 0) profit = '+' + profit;
	$.name.text = data.St.title + ' (' + args.ticker + ')';
	$.data.text = data.St.cot + ' (' + profit + '%)';
	$.graph.image = 'https://chart.finance.yahoo.com/t?s=' + d.St.ticker + '&lang=es-ES&region=ES&width=640&height=360&v=' + Math.round(Math.random() * 1000);
	
	// TODO Temporal
	$.yearly.hide();
	// $.yearly.text = L('yearly') + '\n5%';
	
	var daily_profit = (data.St.cot / data.St.close * 100 - 100).toFixed(2);
	if (daily_profit > 0) daily_profit = '+' + daily_profit;
	$.daily.text = L('daily') + '\n' + daily_profit + '%';
	
	if (data.St) {
		$.cot.text = L('quote_s') + '\n' + convert(data.St.cot);
		$.pos.text = L('position') + '\n' + L(data.St.position);
		var stop = data.St.position == 'long' ? data.St.stop_long : data.St.stop_short;
		stop = parseFloat(stop);
		$.stop.text = L('stop') + '\n' + convert(stop.toFixed(2));
		$.range.text = L('range') + '\n' + convert(data.St.min_prev) + '\n' + convert(data.St.max_prev);
	} else {
		$.scrollview.remove($.st);
	}

	if (!data.Qv || data.Qv.type == 'all') {
		$.go_to_radar.enabled = false;
		$.go_to_radar.opacity = .3;
	}
	$.text.text = data.text;
	$.loading.hide();
	$.scrollview.show();
});

$.historical.addEventListener('click', function() {
	var win = Alloy.createController('history', {ticker: args.ticker, type: 'St'}).getView();
	win.open({left: 0});
});

var d = Ti.UI.createAlertDialog({
	title:L('alert'),
	message:L('reset_wallet'),
	buttonNames:[L('yes'), L('no')],
	cancel:1
});

d.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		if (!haveIt) {
			$.add.title = L('remove_s');
		} else {
			$.add.title = L('add');
		}
		new bbdd({
			ticker:args.ticker,
			model:args.from
		}, 'sts/add');
	}
});

$.add.addEventListener('click', function() {
	d.show();
});

$.analisys.addEventListener('click', function() {
	var win = Alloy.createController('analysis', data).getView();
	win.open({left: 0});
});

$.go_to_radar.addEventListener('click', function() {
	var win = Alloy.createController('qv/finder', data.Qv.type).getView();
	win.open({left: 0});
});
