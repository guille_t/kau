var win = null,
	help = null,
	callback = null,
	backHidden = false;

exports.setTitle = function(newTitle) {
	$.title.text = newTitle;
	$.subheader.remove($.image);
};

exports.setWin = function(w) {
	win = w;
};

exports.hideBack = function(c) {
	if (false && Ti.App.Properties.getInt('demo', true) == false) {
		callback = c;
		$.back.image = '/images/menu.png';
		$.back.height = '20%';
		backHidden = true;
	} else {
		$.back.image = '';
		backHidden = true;
		callback = function() {
			
		};
	}
};

exports.setHelp = function(alert_data) {
	var closeButton = false;
	for (var i in alert_data.buttons) {
		if (alert_data.buttons[i].label == L('close')) {
			closeButton = alert_data.buttons[i].callback;
			alert_data.buttons.pop();
		}
	}
	if (!closeButton) {
		alert_data.buttons.push({
			label: L('close'),
			callback: function() {
				help.animate({opacity:0}, function() {
					win.remove(help);
				});
			}
		});
	} else {
		alert_data.buttons.push({
			label: L('close'),
			callback: function() {
				closeButton();
				help.animate({opacity:0}, function() {
					win.remove(help);
				});
			}
		});
	}
	help = Alloy.createController('alerts', alert_data).getView();
	$.button.visible = true;
};

exports.addItem = function(item) {
	$.subheader.add(item);
};

exports.showHelp = showHelp;

if (OS_ANDROID) {
	//$.header.remove($.close);
}

$.close.addEventListener('singletap', function() {
	if (backHidden) {
		callback();
	} else {
		win.close({left:'100%'});
	}
});

$.button.addEventListener('singletap', showHelp);

function showHelp() {
	win.add(help);
	help.animate({opacity:1});
}