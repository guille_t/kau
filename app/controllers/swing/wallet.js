var args = arguments[0] || {};

if (args == 'model') {
	//$.table.allowsSelection = false;
	var alert_data = {
		header:'Swing Trading',
		title:'Cartera Modelo',
		text:'Nuestra cartera modelo de Swing Trading es un ejemplo de la aplicación de la estrategia de Swing Trading de forma totalmente automática. Esta cartera se compone de 10 valores en los que estamos siempre invertidos y que trabajamos al alza y a la baja. Utilizamos los stops como puntos de swing para entrar largos o cortos en el valor.\n\nPuedes seguir la rentabilidad que obtenemos con ella para descubrir las posibilidades del Swing Trading.\n\nLa rentabilidad que marca la cartera modelo se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo nuestra cartera modelo.',
		buttons:[
			{
				label: L('tutorial'),
				callback: function() {
					var web = Alloy.createController('web', 'http://vimeo.com/112192087').getView();
					web.open({left:0});
				}
			}
		]
	};
	$.header.setHelp(alert_data);
} else {
	$.text.text = L('total_profitability');
	var alert_data = {
		header:'Swing Trading',
		title:'Mi Cartera',
		text:'Crea tu propia cartera de Swing Trading eligiendo los valores que más te gusten. Haz un seguimiento de tus inversiones y de la rentabilidad que obtienes de forma sencilla y efectiva.\n\nLa rentabilidad de mi cartera se actualiza una vez al día a las 8 de la mañana.\n\nAccede a nuestro tutorial para conocer más a fondo las posibilidades de mi cartera.',
		buttons:[
			{
				label: L('tutorial'),
				callback: function() {
					var web = Alloy.createController('web', 'http://vimeo.com/112192088').getView();
					web.open({left:0});
				}
			}
		]
	};
	$.header.setHelp(alert_data);
}

var total = 0;

var bbdd = require('bbdd');

load();
function load() {
	if (args == 'wallet') {
		$.header.setTitle(L('my_wallet') + ' ST');
		new bbdd({type:'my'}, 'stmodels/read', callback);
	} else {
		// $.header.setTitle(L('model_wallet'));
		$.header.setTitle('QV Swing Trading');
		new bbdd({type:'model'}, 'stmodels/read', callback);
	}
}

$.header.setWin($.win);

function callback(data) {
	
	total = data.global_profit;
	data = data.data;
	
	if (data.length == 0) {
		var row = Ti.UI.createTableViewRow({
			height:'100dp',
			backgroundColor:'#F2F2F2'
		});
		row.add(Ti.UI.createLabel({
			width:'90%',
			text:L('no_data_wallet_st'),
			color:'#666',
			textAlign:'center',
			font:{
				fontFamily:Alloy.CFG.fontFamily,
				fontSize:Alloy.CFG.fontSize
			}
		}));
		$.table.appendRow(row);
	} else if (data[0].premium == false) {
		var alert_data = {
			header:'Acceso bloqueado',
			title:'Cuenta premium',
			text:Alloy.CFG.premium_message,
			buttons:[{
				label: L('close'),
				callback: function() {
					not.animate({opacity:0}, function() {
						$.win.remove(not);
					});
				}
			}]
		};
		var not = Alloy.createController('alerts', alert_data).getView();
		$.win.add(not);
		not.animate({opacity:1});
	}
	
	for (var i in data) {
		data[i]._from = args;
		var row = Alloy.createController('swing/row_wallet', data[i]).getView();
		row._data = data[i];
		$.table.appendRow(row);
		//total += parseFloat(data[i].profit);
		//total = data[i].global_profit;
		total += parseFloat(data[i].profit / data.length);
	}
	
	if (total > 0) {
		$.percent.text = '+' + Alloy.Globals.number_format(total, 2, ',', '.') + '%';
		$.percent.color = Alloy.CFG.green;
	} else {
		$.percent.text = Alloy.Globals.number_format(total, 2, ',', '.') + '%';
		$.percent.color = Alloy.CFG.red;
	}
	
}
	
if (args == 'model') {
	$.totals.remove($.reset);
	var place = 3;
} else {
	var place = 4;
}
if (Alloy.CFG.places[place].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = (parseInt(ads.height) + 80) + 'dp';
	$.totals.bottom = ads.height;
}

var confirm = Ti.UI.createAlertDialog({
	title:L('alert'),
	message:L('want_to_reset'),
	buttonNames:[L('yes'), L('no')],
	cancel:1
});
confirm.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		var rows = $.table.sections[0].rows;
		for (var i in rows) {
			$.table.deleteRow(rows[i]);
		}
		new bbdd({model:'St'}, 'wallets/reset', load);
	}
});

$.reset.addEventListener('singletap', function() {
	confirm.show();
});

if (args == 'model') {
	var buttons = [L('graph'), L('historical'), L('cancel')];
} else {
	var buttons = [L('graph'), L('historical'), L('delete'), L('cancel')];
}

var dialog = Ti.UI.createOptionDialog({
	title: L('want_to_do'),
	options: buttons,
	cancel: buttons.length - 1
});

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('view', {ticker: e.row._data.ticker, from: 'St'}).getView();
	win.open({left: 0});
	// dialog._e = e;
	// dialog.show();
});

dialog.addEventListener('click', function(e) {
	if (e.index === e.source.cancel) {
		return;
	}
	switch(e.index) {
		case 0:
			showGraph(this._e);
			break;
		case 1:
			showHistory(this._e);
			break;
		case 2:
			remove(this._e);
			break;
	}
});

function remove(e) {
	if (args == 'model') {
		return;
	}
	var d = Ti.UI.createAlertDialog({
		title:L('remove_action'),
		message:String.format(L('remove_current_action'), e.row._data.name),
		buttonNames:[L('yes'), L('no')],
		cancel:1
	});
	d.show();
	d.addEventListener('click', function(e2) {
		if (e2.index != e2.source.cancel) {
			new bbdd({
				ticker: e.row._data.ticker,
				model:'St'
			}, 'sts/add', function() {
				for (var i = $.table.sections[0].rows.length - 1; i >= 0; i --) {
					$.table.deleteRow(i);
				}
				load();
			});
		}
	});
}

function showGraph(e) {
	var graph = Alloy.createController('graph', {ticker: e.row._data.ticker, type: 'St'}).getView();
	$.win.add(graph);
	graph.addEventListener('singletap', function() {
		$.win.remove(graph);
	});
}

function showHistory(e) {
	var win = Alloy.createController('history', {ticker: e.row._data.ticker, type: 'St'}).getView();
	win.open({left: 0});
}
