var args = arguments[0] || {};

$.header.setWin($.win);
$.header.setTitle(L('add_new_quotes'));

$.table.top = (parseInt(Alloy.CFG.headerHeight) + 50) + 'dp';
$.search.top = (parseInt(Alloy.CFG.headerHeight) + 10) + 'dp';

$.search.addEventListener('return', function() {
	
	$.table.hide();
	$.loading.show();
	
	var url = 'http://d.yimg.com/autoc.finance.yahoo.com/autoc?region=ES&query=' + $.search.value + '&callback=YAHOO.Finance.SymbolSuggest.ssCallback';
	
	var client = Ti.Network.createHTTPClient({
		onload:function() {
			$.table.data = [];
			var result = this.responseText.replace('YAHOO.Finance.SymbolSuggest.ssCallback(', '');
			result = result.substring(0, result.length - 1);
			Ti.API.info(result);
			result = JSON.parse(result);
			var items = result.ResultSet.Result;
			
			if (items.length == 0) {
				Ti.UI.createAlertDialog({
					title:L('alert'),
					message:L('error_yahoo'),
					ok:L('ok')
				}).show();
			}
			
			for (var i in items) {
				var row = Ti.UI.createTableViewRow({
					_symbol:items[i].symbol,
					height:'50dp'
				});
				row.add(Ti.UI.createLabel({
					text:items[i].name + ' (' + items[i].symbol + ')',
					color: "#333",
					font:{
						fontFamily:Alloy.CFG.fontFamily,
						fontSize:Alloy.CFG.fontSizeSmall
					},
					left:'10%',
					right:'10%'
				}));
				$.table.appendRow(row);
			}
			
			$.table.show();
			$.loading.hide();
	
		}
	});
	client.open('GET', url);
	client.send();
	
});

$.table.addEventListener('click', function(e) {
	// Ti.Yahoo.yql('select * from yahoo.finance.quotes where symbol="' + e.row._symbol + '"', function(e) {
		// alert(e);
		// $.win.close();
	// });
	$.table.hide();
	$.loading.show();
	args(e.row._symbol);
});