var args = arguments[0] || {};

var bbdd = require('bbdd');

$.header.setWin($.win);
$.header.setTitle(L('finder'));

var alert_data = {
	header:'Swing Trading',
	title:'Panel de trabajo',
	//text:'Busca las acciones que desees y obtendrás toda la información que necesitas para realizar con éxito la estrategias de Swing Trading en tus inversiones.\n\nRango previsto: te muestra el rango de precios esperado para la siguiente sesión en el valor seleccionado.\n\nPosición: Te muestra la posición en la que se encuentra el valor, (alcista o bajista).\n\nStop: Te muestra el Stop(punto de swing) del valor seleccionado para la siguiente sesión.\n\nSi aún no eres cliente de Kau Finanzas tendrás restringido el uso de los stops swing. Aún así puedes comprar los valores según la tendencia en la que se encuentran y mantenerlos hasta que cambien. Además podrás utilizar el stop swing de los 5 valores que te reglamos.\n\nSi ya eres cliente de Kau Finanzas podrás utilizar los stop swing del valor que desees para conocer el punto en el que las tendencias comienzan y sabrás aprovecharlas todas desde su inicio.\n\nHaz click en mi cartera para añadir el valor a tu cartera\n\nAccede a nuestro tutorial para conocer más a fondo nuestro buscador.',
	text: 'Te presentamos la herramienta de trabajo de KAU+. En nuestro panel encontrarás  todo lo que necesita para operar tus carteras de trading o inversión de medio y largo plazo según los criterios, estilo  y estrategias que estés trabajando con tu coach. Busca la acción que deseas y obtén toda la información que necesitas sobre ella:\n\nCotización: para que sepas a que precio se encuentra actualmente el valor.\nPosición: Te muestra la posición en la que se encuentra el valor, (alcista o bajista) de tal manera que puedas invertir de acuerdo a la tendencia actual.\nStop: Te muestra el Stop del valor seleccionado para la siguiente sesión.\nFundamentales: Te muestra gráficamente si el valor cumple o no con los criterios cuantitativos de KAU+ y es por tanto un buen valor para invertir por fundamentales ya sea en largo o en corto.\n\nAdemás desde el panel de trabajo puedes incluir las acciones de quieras en tu cartera de tal forma que puedas hacer un seguimiento de la rentabilidad que obtendrás con ella.',
	buttons:[
		{
			label: L('tutorial'),
			callback: function() {
				var web = Alloy.createController('web', 'http://vimeo.com/112120621').getView();
				web.open({left:0});
			}
		}
	]
};
$.header.setHelp(alert_data);

var global_profit = 0;

new bbdd({type:'my'}, 'stmodels/read', function(data) {
	for (var i = 0; i < data.data.length; i ++) {
		if (parseInt(data.data[i].profit) != 0) {
			global_profit = 1;
		}
	}
	new bbdd({}, 'sts/get_list', fillTable);
});

function fillTable(data) {
	var alert_showed = false;
	for (var i in data) {
		
		if (alert_showed == false && data[i].premium == false) {
			alert_showed = true;
			var alert_data = {
				header:'Acceso bloqueado',
				title:'Cuenta premium',
				text:Alloy.CFG.premium_message,
				buttons:[{
					label: L('close'),
					callback: function() {
						not.animate({opacity:0}, function() {
							$.win.remove(not);
						});
					}
				}]
			};
			var not = Alloy.createController('alerts', alert_data).getView();
			$.win.add(not);
			not.animate({opacity:1});
		}
		
		var d = {
			haveIt:data[i].St.haveIt,
			ticker:data[i].St.ticker,
			name:data[i].St.title,
			range_from: data[i].premium ? data[i].St.min_prev : data[i].St.y_r_min,
			range_to: data[i].premium ? data[i].St.max_prev : data[i].St.y_r_max,
			stop: data[i].premium ? (data[i].St.position == 'long' ? data[i].St.stop_long : data[i].St.stop_short) : data[i].St.y_stop,
			position: data[i].premium ? data[i].St.position : data[i].St.y_pos,
			cot: data[i].St.cot
		};
		
		if (!Ti.App.Properties.getString('user_id', null) && data[i].St.permanent == false && !Ti.App.Properties.getInt('demo', true)) {
			d.stop = L('premium');
		}
		
		d.global_profit = global_profit;
		
		var rowView = Alloy.createController('swing/row', d);
		row = rowView.getView();
		row._permanent = data[i].St.permanent;
		row._title = data[i].St.title;
		row._ticker = data[i].St.ticker;
		row._data = data[i].St;
		row._callback = rowView.adding;
		$.table.appendRow(row);
	}
}

var search;
$.search.addEventListener('singletap', function() {
	search = Alloy.createController('swing/search', read).getView();
	search.open({left:0});
});

function read(ticker) {
	new bbdd({ticker:ticker}, 'sts/index', callback);
}

function callback(data) {
	search.close({left:'100%'});
	var rows = $.table.sections[0].rows;
	for (var i in rows) {
		$.table.deleteRow(rows[i]);
	}
	new bbdd({
		ticker:data['ticker']
	}, 'follows/add', function() {
		new bbdd({}, 'sts/get_list', fillTable);
	});
}

if (Alloy.CFG.places[2].status) {
	var ads = Alloy.createController('ads').getView();
	$.win.add(ads);
	$.table.bottom = (parseInt(ads.height) + 80) + 'dp';
	$.search.bottom = ads.height;
}

$.table.addEventListener('click', function(e) {
	var win = Alloy.createController('view', {ticker: e.row._data.ticker, from: 'St'}).getView();
	win.open({left: 0});
	return;
	if (e.row._permanent) {
		if (e.row._data.haveIt) {
			var buttons = [L('graph'), L('remove'), L('historical'), L('cancel')];
		} else {
			var buttons = [L('graph'), L('add'), L('historical'), L('cancel')];
		}
	} else {
		if (e.row._data.haveIt) {
			var buttons = [L('graph'), L('remove'), L('historical'), L('delete'), L('cancel')];
		} else {
			var buttons = [L('graph'), L('add'), L('historical'), L('delete'), L('cancel')];
		}
	}
	
	var dialog = Ti.UI.createOptionDialog({
		title: L('want_to_do'),
		options: buttons,
		cancel: buttons.length - 1
	});
	
	dialog._e = e;
	dialog.show();

	dialog.addEventListener('click', function(e) {
		if (e.index === e.source.cancel) {
			return;
		}
		switch(e.index) {
			case 0:
				showGraph(this._e);
				break;
			case 1:
				this._e.row._callback();
				break;
			case 2:
				showHistory(this._e);
				break;
			case 3:
				remove(this._e);
				break;
		}
	});
});

function showGraph(e) {
	var graph = Alloy.createController('graph', {ticker: e.row._ticker, type: 'St'}).getView();
	$.win.add(graph);
	graph.addEventListener('singletap', function() {
		$.win.remove(graph);
	});
}

function showHistory(e) {
	var win = Alloy.createController('history', {ticker: e.row._ticker, type: 'St'}).getView();
	win.open({left: 0});
}

function remove(e) {
	if (e.row._permanent) {
		return;
	}
	confirm.message = String.format(L('delete_row'), e.row._title);
	confirm._selectedRow = e.row;
	confirm.show();
}
var confirm = Ti.UI.createAlertDialog({
	title:L('alert'),
	cancel:1,
	buttonNames:[L('yes'), L('no')]
});
confirm.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		$.table.deleteRow(this._selectedRow);
		new bbdd({
			ticker:this._selectedRow._ticker
		}, 'follows/add');
	}
});
