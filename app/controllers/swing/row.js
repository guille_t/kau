var args = arguments[0] || {};

var bbdd = require('bbdd');

$.name.text = args.name;
$.cot.text = args.cot > 1000 ? Alloy.Globals.number_format(args.cot / 1000, 0, ',', '.') + 'k' : Alloy.Globals.number_format(args.cot, 2, ',', '.');

if (args.range_from > 1000000) {
	var val = Alloy.Globals.number_format(args.range_from / 1000000, 2, ',', '.') + 'm';
} else {
	var val = args.range_from > 1000 ? Alloy.Globals.number_format(args.range_from / 1000, 2, ',', '.') + 'k' : Alloy.Globals.number_format(args.range_from, 2, ',', '.');
}
//$.range_from.text = val;
new bbdd({ticker: args.ticker, model: 'Qv'}, 'sts/get_info', function(d) {
	Ti.API.info(d);
	if (d.Qv && d.Qv.type != 'all') {
		$.fundamentals.image = 'http://kaufinanzas.es/wp-content/themes/kau/img/check.png';
	} else {
		$.fundamentals.image = 'http://kaufinanzas.es/wp-content/themes/kau/img/cross.png';
	}
});

if (args.range_to > 1000000) {
	var val = Alloy.Globals.number_format(args.range_to / 1000000, 2, ',', '.') + 'm';
} else {
	var val = args.range_to > 1000 ? Alloy.Globals.number_format(args.range_to / 1000, 2, ',', '.') + 'k' : Alloy.Globals.number_format(args.range_to, 2, ',', '.');
}
//$.range_to.text = val;
//$.range.text += "\n" + val;

if (args.stop == L('premium')) {
	var val = args.stop;
} else {
	if (args.stop > 1000000) {
		var val = Alloy.Globals.number_format(args.stop / 1000000, 2, ',', '.') + 'm';
	} else {
		var val = args.stop > 1000 ? Alloy.Globals.number_format(args.stop / 1000, 2, ',', '.') + 'k' : Alloy.Globals.number_format(args.stop, 2, ',', '.');
	}
}
$.stop.text = val;

$.position.text = L(args.position + '_t');

if (args.position == 'long') {
	$.position.color = Alloy.CFG.green;
} else {
	$.position.color = Alloy.CFG.red;
}

// if (args.haveIt) {
	// $.add_image.image = '/images/remove.png';
// } else {
	// $.add_image.image = '/images/add.png';
// }

var d = Ti.UI.createAlertDialog({
	title:L('alert'),
	message:L('reset_wallet'),
	buttonNames:[L('yes'), L('no')],
	cancel:1
});

d.addEventListener('click', function(e) {
	if (e.index != e.source.cancel) {
		adding();
	}
});

//$.add.addEventListener('singletap', function() {
exports.adding = function() {
	if (args.global_profit > 0) {
		d.show();
	} else {
		adding();
	}
};
//});

function callback(data) {
	//alert(data);
}

function adding() {
	// if ($.add_image.image == '/images/add.png') {
		// $.add_image.image = '/images/remove.png';
		// $.row._data.haveIt = true;
	// } else {
		// $.add_image.image = '/images/add.png';
		// $.row._data.haveIt = false;
	// }
	
	if ($.row._data.haveIt == false) {
		$.row._data.haveIt = true;
	} else {
		$.row._data.haveIt = false;
	}
	
	new bbdd({
		ticker:args.ticker,
		model:'St'
	}, 'sts/add');
}