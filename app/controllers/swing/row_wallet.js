var args = arguments[0] || {};

$.name.text = args.name;
$.position.text = L(args.position + '_t');
$.cot.text = Alloy.Globals.number_format(args.cot, 2, ',', '.');

if (!args.premium) {
	args.stop = args.y_stop;
	args.position = args.y_pos;
}

if (args._from == 'model') {
	$.date.text = args.date;
	if (!Ti.App.Properties.getString('user_id', null) && !Ti.App.Properties.getInt('demo', true)) {
		$.stop.text = L('premium');
	} else {
		if (args.stop > 1000) {
			$.stop.text = Alloy.Globals.number_format(args.stop, 0, ',', '.');
		} else {
			$.stop.text = Alloy.Globals.number_format(args.stop, 2, ',', '.');
		}
	}
} else {
	if (args.stop > 1000) {
		$.stop.text = Alloy.Globals.number_format(args.stop, 0, ',', '.');
	} else {
		$.stop.text = Alloy.Globals.number_format(args.stop, 2, ',', '.');
	}
}

if (args.position == 'long') {
	$.position.color = Alloy.CFG.green;
} else {
	$.position.color = Alloy.CFG.red;
}

if (args.profit > 0) {
	$.profit.text = '+' + Alloy.Globals.number_format(args.profit, 2, ',', '.') + '%';
	$.profit.color = Alloy.CFG.green;
} else if (args.profit == 0) {
	$.profit.text = 0;
} else {
	$.profit.text = Alloy.Globals.number_format(args.profit, 2, ',', '.') + '%';
	$.profit.color = Alloy.CFG.red;
}
